<?php
namespace AnnonceBundle\Filter;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Lexik\Bundle\FormFilterBundle\Filter\Form\Type as Filters;
use AnnonceBundle\Entity\Annonce;

/**
 * Class CategoryFilterType
 * @package AnnonceBundle\Filter
 */
class CategoryFilterType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $class = $options['class'];
        $innerJoinTable = $options['innerJoinTable'];

        $builder
        ->add('categorie', Filters\EntityFilterType::class, array(
                'class'         => $class,
                'expanded'      => false,
                'multiple'      => false,
                'placeholder'   => 'Tous',
                'query_builder' => function ($repository) {
                  return $repository->createQueryBuilder('c')
                                    ->select('c')
                                    ->where('c.enabled = 1'); },
                'apply_filter'  => function ( $filterQuery, $field, $value) use ($innerJoinTable)
                {
                  $query = $filterQuery->getQueryBuilder();
                  $query->innerJoin($innerJoinTable,'c');
                  $query->where("p.statut = '".Annonce::STATUT_EN_COURS."'");

                  if($value['value'] !== null){
                    $query->andWhere($query->expr()->in('c.id', $value['value']->getId()));
                  }
                },
            ));
    }

    public function getBlockPrefix()
    {
        return 'category_filter';
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'csrf_protection'   => false,
            'validation_groups' => array('filtering') // avoid NotBlank() constraint-related message
        ));
    }
}
