<?php
namespace AnnonceBundle\Filter;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Lexik\Bundle\FormFilterBundle\Filter\Form\Type as Filters;
use AnnonceBundle\Entity\Annonce;

/**
 * Class CategoryFilterType
 * @package AnnonceBundle\Filter
 */
class CategoryProFilterType extends CategoryFilterType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $options['class'] = 'AnnonceBundle:CategoriePro';
        $options['innerJoinTable'] = 'p.categoriePro';
        parent::buildForm($builder, $options);
    }
}
