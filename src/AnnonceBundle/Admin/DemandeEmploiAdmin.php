<?php

namespace AnnonceBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;

class DemandeEmploiAdmin extends AnnonceAdmin
{
    protected $translationDomain = 'SonataAdminBundle';
    /**
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('id')
            ->add('titre')
            ->add('description')
            ->add('codePostal')
            ->add('statut')
            ->add('dateExpiration')
            ->add('telephone')
            ->add('email')
            ->add('dateValidation')
            ->add('messageAdmin')
            ->add('createdAt')
            ->add('updatedAt')
        ;
    }

    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('id')
            ->add('titre')
            ->add('description')
            ->add('codePostal')
            ->add('statut')
            ->add('dateExpiration')
            ->add('telephone')
            ->add('email')
            ->add('dateValidation')
            ->add('messageAdmin')
            ->add('createdAt')
            ->add('updatedAt')
            ->add('_action', null, array(
                'actions' => array(
                    'show' => array(),
                    'edit' => array(),
                    'delete' => array(),
                ),
            ))
        ;
    }

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $em = $this->modelManager->getEntityManager('AnnonceBundle\Entity\CategorieEmploi');

        $query = $em->createQueryBuilder('c')
            ->select('c')
            ->from('AnnonceBundle:CategorieEmploi', 'c')
            ->where('c.enabled = 1');

        parent::configureFormFields($formMapper);
        $formMapper
          ->add('categorieEmploi','sonata_type_model',[
              'multiple' => false,
              'property' => 'name',
              'btn_add'  => false,
              'query'    => $query,
              'label' => $this->trans('label_categorie')
          ])
          ->add('fichiers', 'sonata_type_collection', array(
              'type_options' => array( 'delete' => true )), array(
                'by_reference' => false,
                'edit' => 'inline',
                'inline' => 'table',
                'required' => false,
                'label' => $this->trans('label_fichiers')
          ))
          ;
    }

    /**
     * @param ShowMapper $showMapper
     */
    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->add('id')
            ->add('titre')
            ->add('description')
            ->add('codePostal')
            ->add('statut')
            ->add('dateExpiration')
            ->add('telephone')
            ->add('email')
            ->add('dateValidation')
            ->add('messageAdmin')
            ->add('createdAt')
            ->add('updatedAt')
        ;
    }

    /**
     * @param mixed $object
     */
    public function prePersist($object)
    {
        parent::prePersist($object);
        foreach ($object->getFichiers() as $AnnonceFichiers) {
          $AnnonceFichiers->setFichierName($AnnonceFichiers->getFichier()->getClientOriginalName());
          $AnnonceFichiers->setDemandeEmploi($object);
        }
    }

    /**
     * @param mixed $object
     */
    public function preUpdate($object)
    {
        parent::preUpdate($object);
        foreach ($object->getFichiers() as $AnnonceFichiers) {
          if($AnnonceFichiers->getFichier() != null){
            $AnnonceFichiers->setFichierName($AnnonceFichiers->getFichier()->getClientOriginalName());
            $AnnonceFichiers->setDemandeEmploi($object);
          }
        }
    }
}
