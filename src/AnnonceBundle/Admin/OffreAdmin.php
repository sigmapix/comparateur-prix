<?php

namespace AnnonceBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;

class OffreAdmin extends AnnonceAdmin
{
    protected $translationDomain = 'SonataAdminBundle';
    /**
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('id')
            ->add('titre')
            ->add('description')
            ->add('codePostal')
            ->add('statut')
            ->add('dateExpiration')
            ->add('telephone')
            ->add('email')
            ->add('dateValidation')
            ->add('messageAdmin')
            ->add('createdAt')
            ->add('updatedAt')
            ->add('frais')
            ->add('prix')
            ->add('enchere')
            ->add('prixReserve')
        ;
    }

    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('id')
            ->add('titre')
            ->add('description')
            ->add('codePostal')
            ->add('statut')
            ->add('dateExpiration')
            ->add('telephone')
            ->add('email')
            ->add('dateValidation')
            ->add('messageAdmin')
            ->add('frais')
            ->add('prix')
            ->add('enchere')
            ->add('prixReserve')
            ->add('_action', null, array(
                'actions' => array(
                    'show' => array(),
                    'edit' => array(),
                    'delete' => array(),
                ),
            ))
        ;
    }

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
      $em = $this->modelManager->getEntityManager('AnnonceBundle\Entity\CategoriePro');

      $query = $em->createQueryBuilder('c')
          ->select('c')
          ->from('AnnonceBundle:CategoriePro', 'c')
          ->where('c.enabled = 1');

      parent::configureFormFields($formMapper);
      $formMapper
          ->add('frais',null,[
            'label' => $this->trans('label_frais')
          ])
          ->add('prix',null,[
            'label' => $this->trans('label_prix')
          ])
          ->add('enchere',null,[
            'label' => $this->trans('label_enchere')
          ])
          ->add('prixReserve',null,[
            'label' => $this->trans('label_prixReserve')
          ])
          ->add('categoriePro','sonata_type_model',[
              'multiple' => false,
              'property' => 'name',
              'btn_add'  => false,
              'query'    => $query,
              'label' => $this->trans('label_categorie')
          ])
        ;
    }

    /**
     * @param ShowMapper $showMapper
     */
    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->add('id')
            ->add('titre')
            ->add('description')
            ->add('codePostal')
            ->add('statut')
            ->add('dateExpiration')
            ->add('telephone')
            ->add('email')
            ->add('dateValidation')
            ->add('messageAdmin')
            ->add('createdAt')
            ->add('updatedAt')
            ->add('frais')
            ->add('prix')
            ->add('enchere')
            ->add('prixReserve')
        ;
    }
}
