<?php

namespace AnnonceBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use AnnonceBundle\Entity\Annonce;
use AnnonceBundle\Entity\Offre;
use AnnonceBundle\Entity\OffreEmploi;
use AnnonceBundle\Entity\Demande;
use AnnonceBundle\Entity\DemandeEmploi;

class AnnonceAdmin extends AbstractAdmin
{
    protected $translationDomain = 'SonataAdminBundle';
    /**
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('id')
            ->add('titre')
            ->add('description')
            ->add('codePostal')
            ->add('statut')
            ->add('dateExpiration')
            ->add('telephone')
            ->add('email')
            ->add('dateValidation')
            ->add('messageAdmin')
            ->add('createdAt')
            ->add('updatedAt')
        ;
    }

    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('id')
            ->add('titre')
            ->add('description')
            ->add('codePostal')
            ->add('statut')
            ->add('dateExpiration')
            ->add('telephone')
            ->add('email')
            ->add('dateValidation')
            ->add('messageAdmin')
            ->add('createdAt')
            ->add('updatedAt')
            ->add('_action', null, array(
                'actions' => array(
                    'show' => array(),
                    'edit' => array(),
                    'delete' => array(),
                ),
            ))
        ;
    }

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
      $annonce = $this->getSubject();
      $choices = $this->getChoices($annonce);

        $formMapper
            ->add('titre',null,[
              'label' => $this->trans('label_titre')
            ])
            ->add('description',null,[
              'label' => $this->trans('label_description')
            ])
            ->add('auteur',null,[
              'label' => $this->trans('label_auteur')
            ])
            ->add('statut', 'choice', array(
                'required' => true,
                'choices' => $choices,
                'label' => $this->trans('label_statut'))
                )
            ->add('images', 'sonata_type_collection', array(
                'type_options' => array( 'delete' => true )), array(
                  'by_reference' => true,
                  'edit' => 'inline',
                  'inline' => 'table',
                  'required' => false,
                  'label' => $this->trans('label_image')
            ))
            ->add('codePostal',null,[
              'label' => $this->trans('label_code_postal')
            ])
            ->add('duree',null,[
              'label' => $this->trans('label_duree')
            ])
            ->add('telephone',null,[
              'label' => $this->trans('label_telephone')
            ])
            ->add('email',null,[
              'label' => $this->trans('label_email')
            ])
            ->add('messageAdmin',null,[
              'label' => $this->trans('label_message_admin')
            ])
        ;


    }

    /**
     * @param ShowMapper $showMapper
     */
    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->add('id')
            ->add('titre')
            ->add('description')
            ->add('codePostal')
            ->add('statut')
            ->add('dateExpiration')
            ->add('telephone')
            ->add('email')
            ->add('dateValidation')
            ->add('messageAdmin')
            ->add('createdAt')
            ->add('updatedAt')
        ;
    }

    /**
     * @param mixed $object
     */
    public function prePersist($object)
    {
        $duree = $object->getDuree();
        $today = new \DateTime();
        $today->add(new \DateInterval('P'.$duree.'D'));
        $object->setDateExpiration($today);

        if($object->getStatut() === Annonce::STATUT_VALIDEE){
            $object->setStatut(Annonce::STATUT_EN_ATTENTE_VALIDATION);
            $workflow->apply($object, Annonce::STATUT_VALIDEE);
            $object->setDateValidation(new \DateTime());
        }

        foreach ($object->getImages() as $AnnonceImages) {
          $AnnonceImages->setImageName($AnnonceImages->getImage()->getClientOriginalName());
          $AnnonceImages->setAnnonces($object);
        }
    }

    /**
     * @param mixed $object
     */
    public function preUpdate($object)
    {
        $workflow = $this->getConfigurationPool()->getContainer()->get('workflow.annonce_publishing');

        //if validation date is null, then we can safely assume that
        //the previous state was "en_attente_validation" or "Brouillon"
        if($object->getDateExpiration() === null){

          /* if the new status is "validee" OR "refusee", this would imply that the previous state was "en_attente_validation"
           * during the update, the status is SET to the new one but is not APPLY (from workflow)
           * when the new status is simply set, there are no workflow events which are triggered
           * therefore we have to revert the status update in that special case
           * then we run the "apply" method which correctly triggers the associated event..
           */
            if($object->getStatut() === Annonce::STATUT_VALIDEE){
                $object->setStatut(Annonce::STATUT_EN_ATTENTE_VALIDATION);
                $workflow->apply($object, Annonce::STATUT_VALIDEE);
                $object->setDateValidation(new \DateTime());
            }

            if($object->getStatut() === Annonce::STATUT_REFUSEE){
                $object->setStatut(Annonce::STATUT_EN_ATTENTE_VALIDATION);
                $workflow->apply($object, Annonce::STATUT_REFUSEE);
            }
        }

        /*
         * we recalculate the duree only if status is validee or en_cours
         * otherwise this should not have any impact and therefore set to null
         */
        if($object->getStatut() === Annonce::STATUT_VALIDEE || $object->getStatut() === Annonce::STATUT_EN_COURS){
            $duree = $object->getDuree();
            $today = new \DateTime();
            $today->add(new \DateInterval('P'.$duree.'D'));
            if($object->getDateExpiration() !== $today){
                $object->setDateExpiration($today);
            }
        }
        else {
            $object->setDateExpiration(null);
        }

        if($object->getStatut() === Annonce::STATUT_EXPIREE){
            $object->setStatut(Annonce::STATUT_EN_COURS);
            $workflow->apply($object, Annonce::STATUT_EXPIREE);
        }


        //if there are new images, add them
        foreach ($object->getImages() as $AnnonceImages) {
          if($AnnonceImages->getImage() != null){
            $AnnonceImages->setImageName($AnnonceImages->getImage()->getClientOriginalName());
            $AnnonceImages->setAnnonces($object);
          }
        }
    }

    //retrieve the available choices based on the current status of the annonce
    private function getChoices($annonce)
    {
      $workflow = $this->getConfigurationPool()->getContainer()->get('workflow.annonce_publishing');

      //we retrieve the current workflow "place"
      $current = key($workflow->getMarking($annonce)->getPlaces());

      //we initialize the available choices to an empty array
      $choices = array();

      //we add the current place
      $choices[$current] = $current;

      //using the function available in workflow, we retrieve the available "transitions"
      $transitions = $workflow->getEnabledTransitions($annonce);

      //we loop over the available transitions
      //we retrieve the "To" of that transition
      //we then assign that To as a choice
      foreach ($transitions as $key => $transition) {
        $choices[$transition->getTos()[0]] = $transition->getTos()[0];
      }
      return $choices;
    }
}
