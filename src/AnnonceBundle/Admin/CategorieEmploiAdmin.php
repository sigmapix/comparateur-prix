<?php

namespace AnnonceBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;

class CategorieEmploiAdmin extends AbstractAdmin
{
    protected $translationDomain = 'SonataAdminBundle';
    /**
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('name')
            ->add('enabled')
            ->add('slug')
            ->add('description')
            ->add('position')
            ->add('createdAt')
            ->add('updatedAt')
            ->add('id')
        ;
    }

    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('name')
            ->add('enabled')
            ->add('slug')
            ->add('description')
            ->add('position')
            ->add('createdAt')
            ->add('updatedAt')
            ->add('id')
            ->add('_action', null, array(
                'actions' => array(
                    'show' => array(),
                    'edit' => array(),
                    'delete' => array(),
                ),
            ))
        ;
    }

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
      $formMapper
        ->add('name',null,[
            'label' => $this->trans('label_nom')
        ])
        ->add('enabled',null,[
            'required' => false,
            'label' => $this->trans('label_actif')
        ])
        ->add('description',null,
            ['label' => $this->trans('label_description')]
        );

    }

    /**
     * @param ShowMapper $showMapper
     */
    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->add('name')
            ->add('enabled')
            ->add('slug')
            ->add('description')
            ->add('position')
            ->add('createdAt')
            ->add('updatedAt')
            ->add('id')
        ;
    }
}
