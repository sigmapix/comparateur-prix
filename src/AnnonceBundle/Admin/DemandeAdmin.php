<?php

namespace AnnonceBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;

class DemandeAdmin extends AnnonceAdmin
{
    protected $translationDomain = 'SonataAdminBundle';
    /**
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('id')
            ->add('titre')
            ->add('description')
            ->add('codePostal')
            ->add('statut')
            ->add('dateExpiration')
            ->add('telephone')
            ->add('email')
            ->add('dateValidation')
            ->add('messageAdmin')
            ->add('createdAt')
            ->add('updatedAt')
        ;
    }

    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('id')
            ->add('titre')
            ->add('description')
            ->add('codePostal')
            ->add('statut')
            ->add('dateExpiration')
            ->add('telephone')
            ->add('email')
            ->add('dateValidation')
            ->add('messageAdmin')
            ->add('createdAt')
            ->add('updatedAt')
            ->add('_action', null, array(
                'actions' => array(
                    'show' => array(),
                    'edit' => array(),
                    'delete' => array(),
                ),
            ))
        ;
    }

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
      $em = $this->modelManager->getEntityManager('AnnonceBundle\Entity\CategoriePro');

      $query = $em->createQueryBuilder('c')
          ->select('c')
          ->from('AnnonceBundle:CategoriePro', 'c')
          ->where('c.enabled = 1');

        parent::configureFormFields($formMapper);
        $formMapper->add('categoriePro','sonata_type_model',[
            'multiple' => false,
            'property' => 'name',
            'btn_add'  => false,
            'query'    => $query,
            'label' => $this->trans('label_categorie')
        ]);

    }

    /**
     * @param ShowMapper $showMapper
     */
    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->add('id')
            ->add('titre')
            ->add('description')
            ->add('codePostal')
            ->add('statut')
            ->add('dateExpiration')
            ->add('telephone')
            ->add('email')
            ->add('dateValidation')
            ->add('messageAdmin')
            ->add('createdAt')
            ->add('updatedAt')
        ;
    }
}
