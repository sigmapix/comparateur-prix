<?php
namespace AnnonceBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

class DemandeEmploiType extends AnnonceType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder,  $options);
        $builder
            ->add('categorieEmploi', EntityType::class, array(
              'class' => 'AnnonceBundle:CategorieEmploi',
              'choice_label' => function($categorieEmploi){
                    return $categorieEmploi->getName();
                },
              'label' => 'label_categorie'
            ))
            ->add('fichiers', FileType::class, array(
                  'multiple'   => true,
                  'required'   => false,
                  'label' => 'label_fichiers'
              ))
            ->add('save',         SubmitType::class)
            ;
    }
}
