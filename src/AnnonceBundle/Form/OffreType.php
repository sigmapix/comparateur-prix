<?php
namespace AnnonceBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

class OffreType extends AnnonceType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder,  $options);
        $builder
            ->add('frais',        IntegerType::class, array(
                'required'   => false,
                'label' => 'label_frais'
            ))
            ->add('prix',         IntegerType::class, array(
                'label' => 'label_prix'
            ))
            ->add('enchere',      CheckboxType::class, array(
                'required'   => false,
                'label' => 'label_enchere'
            ))
            ->add('prixReserve',  IntegerType::class, array(
                'required'   => false,
                'label' => 'label_prixReserve'
            ))
            ->add('categoriePro', EntityType::class, array(
              'class' => 'AnnonceBundle:CategoriePro',
              'choice_label' => function($categoriePro){
                    return $categoriePro->getName();
                },
              'label' => 'label_categorie'
            ))
            ->add('save',         SubmitType::class)
          ;

    }
}
