<?php
namespace AnnonceBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;


class AnnonceType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('titre',        TextType::class,[
              'label' => 'label_titre'
            ])
            ->add('description',  TextareaType::class,[
              'label' => 'label_description'
            ])
            ->add('codePostal',   TextType::class,[
              'label' => 'label_code_postal'
            ])
            ->add('telephone',    TextType::class, array(
                'required'   => false,
                'label' => 'label_telephone'
            ))
            ->add('email',        EmailType::class, array(
                'required'   => false,
                'label' => 'label_email'
            ))
            ->add('duree',        IntegerType::class)
            ->add('images',       FileType::class, array(
                  'multiple'   => true,
                  'required'   => false,
                  'label' => 'label_image'
              ))
              ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'translation_domain' => 'SonataAdminBundle'
        ));
    }
}
