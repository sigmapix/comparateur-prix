<?php

namespace AnnonceBundle\Manager;

use AnnonceBundle\Entity\Annonce;
use AnnonceBundle\Entity\Offre;
use AnnonceBundle\Entity\Enchere;
use AnnonceBundle\Entity\OffreEmploi;
use AnnonceBundle\Entity\Demande;
use AnnonceBundle\Entity\DemandeEmploi;
use AnnonceBundle\Entity\DemandeEmploiFichier;
use AnnonceBundle\Entity\AnnonceImage;
use AnnonceBundle\Entity\CategoriePro;
use AnnonceBundle\Entity\CategorieEmploi;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;


class AnnonceManager
{

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     * @param $objet
     * @return bool
     */
    public function persistAndFlush($objet, $flush = false)
    {
        if (!is_object($objet)) {
            return false;
        }

        $this->em->persist($objet);
        if($flush){
            $this->em->flush();
        }

    }

    public function removeEntity($objet)
    {
        $this->em->remove($objet);
    }

    /**
     * @param $type
     * @return \AnnonceBundle\Repository\DemandeEmploiRepository|\AnnonceBundle\Repository\DemandeRepository|\AnnonceBundle\Repository\OffreEmploiRepository|\AnnonceBundle\Repository\OffreRepository|\Doctrine\ORM\EntityRepository
     */
    public function getRepository($type)
    {
        $repository = $this->em->getRepository($type);

        return $repository;
    }

    public function getMesAnnonces($type, $user ,$order = 'DESC')
    {
        return $this->getRepository($type)->findBy(['auteur' => $user->getId()],['createdAt' => $order]);
    }

    /**
     * @param $id
     * @return null|object
     */
    public function get($id)
    {
        if(!$id){
            return null;
        }

        return $this->getRepository()->find($id);
    }



    public function persistAnnonce($user, $request, $annonce, $images=null, $fichiers=null)
    {

        $annonce->setAuteur($user);
        $annonce->setStatut(Annonce::STATUT_EN_ATTENTE_VALIDATION);

        if($annonce instanceof Demande){
          $days = $request->get('demande')['duree'];
        }

        if($annonce instanceof DemandeEmploi){
          $days = $request->get('demande_emploi')['duree'];

          //add new files if present
          foreach($fichiers as $fichier){
            if ($fichier !== null){
              $annonceFichier = new DemandeEmploiFichier();
              $annonceFichier->setFichier($fichier);
              $annonceFichier->setFichierName($fichier->getClientOriginalName());
              $annonceFichier->setDemandeEmploi($annonce);
              $annonce->addFichier($annonceFichier);
              $this->em->persist($annonceFichier);
            }
          }
        }

        if($annonce instanceof Offre){
          $days = $request->get('offre')['duree'];
        }

        if($annonce instanceof   OffreEmploi){
          $days = $request->get('offre_emploi')['duree'];
        }

        $today = new \DateTime();
        $today->add(new \DateInterval('P'.$days.'D'));
        $annonce->setDateExpiration($today);
        //if we are persisting the annonce, we set validation date to null
        //used in case of "republie" or "modifier"
        $annonce->setDateValidation(null);


        //if new images were added, add them
        foreach($images as $image){
          if ($image !== null){
            $annonceImage = new AnnonceImage();
            $annonceImage->setImage($image);
            $annonceImage->setImageName($image->getClientOriginalName());
            $annonceImage->setAnnonces($annonce);
            $annonce->addImage($annonceImage);
            $this->em->persist($annonceImage);
          }
        }
        $this->persistAndFlush($annonce, true);
    }

    public function persistEnchere($user, $annonce, $form)
    {
      $enchere = new Enchere();
      $enchere->setEncherisseur($user);
      $enchere->setOffre($annonce);
      $enchere->setMontant($form['montant']);
      $this->persistAndFlush($enchere,true);
    }

    public function getEncheresOverPrixReserve($annonce){
        $repositoryEnchere =  $this->getRepository(Enchere::class);
        $encheres = $repositoryEnchere->getEncheresOverPrixReserve($annonce);
        return $encheres;
    }

    public function getEnchereGagnant($annonce){
        $repositoryEnchere =  $this->getRepository(Enchere::class);
        $enchere = $repositoryEnchere->getEnchereGagnant($annonce);
        return $enchere;
    }
}
