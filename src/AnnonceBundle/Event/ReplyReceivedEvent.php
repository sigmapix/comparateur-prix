<?php

namespace AnnonceBundle\Event;

use Symfony\Component\EventDispatcher\Event;
use Symfony\Component\HttpFoundation\Request;
use AnnonceBundle\Entity\Annonce;

class ReplyReceivedEvent extends Event
{
    const NAME = 'annonce.reply.received';

    protected $annonce;
    protected $request;
    protected $user;

    public function __construct(Request $request, Annonce $annonce, $user)
    {
      $this->annonce = $annonce;
      $this->request = $request;
      $this->user = $user;
    }

    public function getAnnonce()
    {
        return $this->annonce;
    }

    public function getRequest()
    {
        return $this->request;
    }

    public function getUser()
    {
        return $this->user;
    }
}
