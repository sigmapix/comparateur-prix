<?php

namespace AnnonceBundle\Services;


use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Bundle\FrameworkBundle\Templating\EngineInterface as Templating;
use AnnonceBundle\Manager\AnnonceManager;

class Mailer
{
    const MAIL_VALIDATION = 1;
    const MAIL_REJECTION = 2;
    const MAIL_ENCHERE = 3;

    /**
     * @var \Swift_Mailer
     */
    protected $mailer;

    /**
     * @var UrlGeneratorInterface
     */
    protected $router;

    /**
     * @var EngineInterface
     */
    protected $templating;

    /**
     * @var AnnonceManager
     */
    protected $annonceManager;

    public function __construct(\Swift_Mailer $mailer, Templating $templating, RouterInterface $router, AnnonceManager $annonceManager)
    {
        $this->mailer = $mailer;
        $this->router = $router;
        $this->templating = $templating;
        $this->annonceManager = $annonceManager;
    }

    /**
     * @param string       $renderedTemplate
     * @param array|string $fromEmail
     * @param array|string $toEmail
     */
    public function sendEmailMessage($toEmail, $replyTo = null, $title, $body, $fichiers=null)
    {
        $message = (new \Swift_Message($title))
            ->setFrom('esokia.sigmapix@gmail.com')
            ->setReplyTo($replyTo)
            ->setTo($toEmail)
            ->setBody(
                $this->templating->render(
                    'AnnonceBundle:Annonce:email.html.twig',
                    array('title' => $title , 'message' => $body)
                  ),
                  'text/html'
            )
        ;
        if($fichiers !== null){
          $folder = 'uploadedfile';
          $fileName = round(microtime(true)) . $fichiers->getClientOriginalName();
          if(!file_exists($folder. '/' . $fileName)){
            $fichiers->move($folder, $fileName);
          }
          $message->attach(\Swift_Attachment::fromPath($folder. '/' . $fileName));
        }

        $this->mailer->send($message);
    }

    public function sendEmailEnchere($toEmail, $title, $encheres, $enchereGagnant)
    {
        $message = (new \Swift_Message($title))
            ->setFrom('esokia.sigmapix@gmail.com')
            ->setTo($toEmail)
            ->setBody(
                $this->templating->render(
                    'AnnonceBundle:Annonce:emailEnchere.html.twig',
                    array('title' => $title , 'encheres' => $encheres, 'enchereGagnant' => $enchereGagnant)
                  ),
                  'text/html'
            )
        ;

        $this->mailer->send($message);
    }

    public function sendEmailByType($type, $annonce){
        switch($type){
          case SELF::MAIL_VALIDATION:
              $this->sendEmailValidation($annonce);
              break;
          case SELF::MAIL_REJECTION:
              $this->sendEmailRejection($annonce);
              break;
          case SELF::MAIL_ENCHERE:
              $this->sendEmailRecapitulatif($annonce);
              break;
        }
    }

    private function sendEmailValidation($annonce)
    {
        //send email of annonce validation to author
        $title = "Votre annonce a ete valider";
        $emailAuteur = $annonce->getAuteur()->getEmail();
        $body = "Annonce valider";
        $this->sendEmailMessage($emailAuteur, null, $title, $body);
    }

    private function sendEmailRejection($annonce)
    {
        //send email of annonce invalidation to author
        $title = "Votre annonce n'a pas ete valider";
        $emailAuteur = $annonce->getAuteur()->getEmail();
        $body = "Annonce NON valider";
        $this->mailer->sendEmailMessage($emailAuteur, null, $title, $body);
    }

    private function sendEmailRecapitulatif($annonce)
    {
      $emailAuteur = $annonce->getAuteur()->getEmail();

      $encheres = $this->annonceManager->getEncheresOverPrixReserve($annonce);
      $enchereGagnant = $this->annonceManager->getEnchereGagnant($annonce);

      //dump($encheres);dump($enchereGagnant);die();
      $title = "Encheres sur votre annonce";
      $this->sendEmailEnchere($emailAuteur, $title, $encheres, $enchereGagnant);
    }
}
