<?php

namespace AnnonceBundle\EventSubscriber;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use AnnonceBundle\Services\Mailer;
use AnnonceBundle\Event\ReplyReceivedEvent;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class ReplyReceivedSubscriber implements EventSubscriberInterface
{
    private $mailer;
    private $router;

    public function __construct(Mailer $mailer, RouterInterface $router)
    {
        $this->mailer = $mailer;
        $this->router = $router;
    }

    public function onReceived(ReplyReceivedEvent $event)
    {
        $annonce = $event->getAnnonce();
        $request = $event->getRequest();
        $user = $event->getUser();

        $form = $request -> get('reply');
        $fichiers = $request->files->get('reply')['fichier'];

        $body = $this->generateBody($form,$annonce);
        //send emails
        $this->sendMailToUser($user, $body);

        $auteur = $annonce->getAuteur();
        $this->sendMailToAuteur($user, $auteur, $body, $fichiers);
    }

    public static function getSubscribedEvents()
    {
        return array(
             ReplyReceivedEvent::NAME => 'onReceived',
        );
    }

    private function generateBody($form,$annonce)
    {
      $body = $form['reponse'];
      if (isset($form['prix'])){
        $body .= "<br />" . $form['prix'];
      }

      $lien = $this->router->generate('annonce_show',['id' => $annonce->getId()], UrlGeneratorInterface::ABSOLUTE_URL);
      $body .= "<br />lien:" . $lien;
      return $body;
    }

    private function sendMailToUser($user, $body)
    {
      //send email to respondee
      $emailUser = $user->getEmail();
      $title = "Votre reponse a bien ete envoyer";
      $this->mailer->sendEmailMessage($emailUser, null, $title, $body);
    }

    private function sendMailToAuteur($user, $auteur, $body, $fichiers)
    {
      //send email to auteur
      $emailUser = $user->getEmail();
      $auteurEmail = $auteur->getEmail();
      $title = "Vous avez une reponse";
      $this->mailer->sendEmailMessage($auteurEmail, $emailUser, $title, $body, $fichiers);
    }
}
