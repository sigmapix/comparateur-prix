<?php

namespace AnnonceBundle\EventSubscriber;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Workflow\Event\Event;
use Symfony\Bundle\FrameworkBundle\Templating\EngineInterface as Templating;
use AnnonceBundle\Services\Mailer;
use AnnonceBundle\Entity\Offre;
use AnnonceBundle\Entity\Enchere;
use AnnonceBundle\Manager\AnnonceManager;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class AnnonceStatusChangedSubscriber implements EventSubscriberInterface
{
    private $mailer;
    private $templating;
    private $annonceManager;

    public function __construct(Mailer $mailer, Templating $templating, AnnonceManager $annonceManager)
    {
      $this->mailer = $mailer;
      $this->templating = $templating;
      $this->annonceManager = $annonceManager;
    }

    public function onValidate(Event $event)
    {
        $annonce = $event->getSubject();
        $this->mailer->sendEmailByType(Mailer::MAIL_VALIDATION, $annonce);
    }

    public function onReject(Event $event)
    {
        $annonce = $event->getSubject();
        $this->mailer->sendEmailByType(Mailer::MAIL_REJECTION, $annonce);
    }

    public function onExpired(Event $event)
    {
        $annonce = $event->getSubject();
        if($annonce instanceof Offre){
            if($annonce->getEnchere()){
              $this->mailer->sendEmailByType(Mailer::MAIL_ENCHERE, $annonce);
            }
        }
    }

    public static function getSubscribedEvents()
    {

        return array(
            'workflow.annonce_publishing.enter.validee' => 'onValidate',
            'workflow.annonce_publishing.enter.refusee' => 'onReject',
            'workflow.annonce_publishing.enter.expiree' => 'onExpired',
        );
    }

}
