<?php
namespace AnnonceBundle\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use AnnonceBundle\Entity\Annonce;
use AnnonceBundle\Entity\Offre;
use AnnonceBundle\Entity\Enchere;

class UpdateStatusCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
        ->setName('annonce:update_status')

        ->setDescription('Mise a jour des statut des annonces')

        ->setHelp('Cette commande mets a jour les status des annonces')
      ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln('Debut de la mise a jour des status de tous les annonces!');
        $em = $this ->getContainer()
                    ->get('doctrine')
                    ->getManager();
        $repository =  $em ->getRepository('AnnonceBundle:Annonce');
        $workflow = $this->getContainer()->get('workflow.annonce_publishing');
        //update all annonces from en_cours to expired if required
        $annonces = $repository->findby(
          array('statut' => Annonce::STATUT_EN_COURS)
        );

        $today = new \DateTime();
        foreach ($annonces as $key => $annonce) {
            if($annonce->getDateExpiration() < $today){
                if($workflow->can($annonce, Annonce::STATUT_EXPIREE)){
                    $workflow->apply($annonce, Annonce::STATUT_EXPIREE);
                    $annonce->setDateExpiration(null);
                    $em->persist($annonce);
                }
            }
        }

        //change all annonce from validee to en cours
        $annonces = $repository->findby(
            array('statut' => Annonce::STATUT_VALIDEE)
        );

        $today = new \DateTime();
        foreach ($annonces as $key => $annonce) {
            if($workflow->can($annonce, Annonce::STATUT_EN_COURS)){
              $workflow->apply($annonce, Annonce::STATUT_EN_COURS);
              if($annonce->getDateExpiration() < $today){
                $workflow->apply($annonce, Annonce::STATUT_EXPIREE);
              }

                $em->persist($annonce);
            }
        }
        $em->flush();
        $output->writeln('Fin de la mise a jour des status de tous les annonces!');
    }
}
