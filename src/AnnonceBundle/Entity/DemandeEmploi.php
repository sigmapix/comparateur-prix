<?php

namespace AnnonceBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * DemandeEmploi
 *
 * @ORM\Table(name="demande_emploi")
 * @ORM\Entity(repositoryClass="AnnonceBundle\Repository\DemandeEmploiRepository")
 */
class DemandeEmploi extends Annonce
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var CategoryEmploi
     * @ORM\ManyToOne(targetEntity="CategorieEmploi", inversedBy="demandesEmploi")
     * @ORM\JoinColumn(name="categorie_emploi_id", referencedColumnName="id")
     */
    private $categorieEmploi;

    /**
     * @var DemandeEmploiFichier
     *
     *  @ORM\OneToMany(targetEntity="AnnonceBundle\Entity\DemandeEmploiFichier", mappedBy="demandeEmploi", cascade={"persist", "remove"},orphanRemoval=true)
     */
    private $fichiers;



    /**
     * Set categorieEmploi
     *
     * @param \AnnonceBundle\Entity\CategorieEmploi $categorieEmploi
     *
     * @return DemandeEmploi
     */
    public function setCategorieEmploi(\AnnonceBundle\Entity\CategorieEmploi $categorieEmploi = null)
    {
        $this->categorieEmploi = $categorieEmploi;

        return $this;
    }

    /**
     * Get categorieEmploi
     *
     * @return \AnnonceBundle\Entity\CategorieEmploi
     */
    public function getCategorieEmploi()
    {
        return $this->categorieEmploi;
    }

    /**
     * Add fichier
     *
     * @param \AnnonceBundle\Entity\DemandeEmploiFichier $fichier
     *
     * @return DemandeEmploi
     */
    public function addFichier(\AnnonceBundle\Entity\DemandeEmploiFichier $fichier)
    {
        $this->fichiers[] = $fichier;

        return $this;
    }

    /**
     * Remove fichier
     *
     * @param \AnnonceBundle\Entity\DemandeEmploiFichier $fichier
     */
    public function removeFichier(\AnnonceBundle\Entity\DemandeEmploiFichier $fichier)
    {
        $this->fichiers->removeElement($fichier);
    }

    /**
     * Get fichiers
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getFichiers()
    {
        return $this->fichiers;
    }
}
