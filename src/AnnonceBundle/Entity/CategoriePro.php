<?php

namespace AnnonceBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Sonata\ClassificationBundle\Entity\BaseCategory as BaseCategory;

/**
 * Category
 *
 * @ORM\Table(name="category_pro")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\CategoryRepository")
 */
class CategoriePro extends BaseCategory
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\OneToMany(targetEntity="Demande", mappedBy="categoriePro")
     * @ORM\JoinColumn(nullable=true)
     */
    private $demandes;

    /**
     * @ORM\OneToMany(targetEntity="Offre", mappedBy="categoriePro")
     * @ORM\JoinColumn(nullable=true)
     */
    private $offres;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->demandes = new \Doctrine\Common\Collections\ArrayCollection();
        $this->offres = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * @param mixed $object
     * @return string
     *
     */
    public function toString($object)
    {
        return $object instanceof CategorieEmploi
            ? $object->getNom()
            : 'Categorie'; // shown in the breadcrumb on the create view
    }

    /**
     * Add demande
     *
     * @param \AnnonceBundle\Entity\Demande $demande
     *
     * @return CategoriePro
     */
    public function addDemande(\AnnonceBundle\Entity\Demande $demande)
    {
        $this->demandes[] = $demande;

        return $this;
    }

    /**
     * Remove demande
     *
     * @param \AnnonceBundle\Entity\Demande $demande
     */
    public function removeDemande(\AnnonceBundle\Entity\Demande $demande)
    {
        $this->demandes->removeElement($demande);
    }

    /**
     * Get demandes
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getDemandes()
    {
        return $this->demandes;
    }

    /**
     * Add offre
     *
     * @param \AnnonceBundle\Entity\Offre $offre
     *
     * @return CategoriePro
     */
    public function addOffre(\AnnonceBundle\Entity\Offre $offre)
    {
        $this->offres[] = $offre;

        return $this;
    }

    /**
     * Remove offre
     *
     * @param \AnnonceBundle\Entity\Offre $offre
     */
    public function removeOffre(\AnnonceBundle\Entity\Offre $offre)
    {
        $this->offres->removeElement($offre);
    }

    /**
     * Get offres
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getOffres()
    {
        return $this->offres;
    }
}
