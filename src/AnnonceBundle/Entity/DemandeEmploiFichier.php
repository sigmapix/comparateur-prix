<?php

namespace AnnonceBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use Symfony\Component\HttpFoundation\File\File;

/**
 *
 * @Vich\Uploadable
 *
 * @ORM\Table(name="demande_emploi_fichier")
 * @ORM\Entity(repositoryClass="AnnonceBundle\Repository\DemandeEmploiFichierRepository")
 */
class DemandeEmploiFichier
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @Assert\File(maxSize="2M")
     * @Vich\UploadableField(mapping="annonce_fichier", fileNameProperty="fichierName")
     *
     *
     * @var File
     */
    private $fichier;

    /**
     * @ORM\Column(type="string", length=255)
     *
     * @var string
     */
    private $fichierName;

    /**
      * @var Annonce
      *
      * @ORM\ManyToOne(targetEntity="AnnonceBundle\Entity\DemandeEmploi", inversedBy="fichiers")
      * @ORM\JoinColumn(nullable=false)
      */
    private $demandeEmploi;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     *
     * @param File|\Symfony\Component\HttpFoundation\File\UploadedFile $fichier
     *
     * @return DemandeEmploi
     */
    public function setFichier(File $fichier = null)
    {
        $this->fichier = $fichier;

        return $this;
    }

    /**
     * @return File|null
     */
    public function getFichier()
    {
        return $this->fichier;
    }

    /**
     * @param string $fichierName
     *
     * @return DemandeEmploi
     */
    public function setFichierName($fichierName)
    {
        $this->fichierName = $fichierName;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getFichierName()
    {
        return $this->fichierName;
    }

    /**
     * Set demandeEmploi
     *
     * @param \AnnonceBundle\Entity\DemandeEmploi $demandeEmploi
     *
     * @return DemandeEmploiFichier
     */
    public function setDemandeEmploi(\AnnonceBundle\Entity\DemandeEmploi $demandeEmploi)
    {
        $this->demandeEmploi = $demandeEmploi;

        return $this;
    }

    /**
     * Get demandeEmploi
     *
     * @return \AnnonceBundle\Entity\DemandeEmploi
     */
    public function getDemandeEmploi()
    {
        return $this->demandeEmploi;
    }
}
