<?php

namespace AnnonceBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Annonce
 *
 * @ORM\Table(name="annonces")
 * @ORM\Entity(repositoryClass="AnnonceBundle\Repository\AnnonceRepository")
 * @ORM\InheritanceType("SINGLE_TABLE")
 * @ORM\DiscriminatorColumn( name = "discr", type = "string" )
 * @ORM\DiscriminatorMap({ "demande" = "Demande","offre" = "Offre","offre_emploi" = "OffreEmploi","demande_emploi"="DemandeEmploi" })
 */
abstract class Annonce
{
    const DEFAULT_PERIOD_OF_VALIDITY = 90;

    //declaring the constants for status of annonce
    const STATUT_BROUILLON = "brouillon";
    const STATUT_EN_ATTENTE_VALIDATION = "en_attente_validation";
    const STATUT_EN_COURS = "en_cours";
    const STATUT_VALIDEE = "validee";
    const STATUT_REFUSEE = "refusee";
    const STATUT_ARRETEE = "arretee";
    const STATUT_EXPIREE = "expiree";

    use TimestampableEntity;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="titre", type="string", length=255)
     */
    protected $titre;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text")
     */
    protected $description;

    /**
     *
     * @ORM\OneToMany(targetEntity="AnnonceBundle\Entity\AnnonceImage", mappedBy="annonces", cascade={"persist", "remove"},orphanRemoval=true)
     *
     */
    protected $images;

    /**
     * @var User $auteur
     *
     * @Gedmo\Blameable(on="change")
     * @ORM\ManyToOne(targetEntity="Application\Sonata\UserBundle\Entity\User", inversedBy="annonces")
     * @ORM\JoinColumn(referencedColumnName="id")
     */
    protected $auteur;

    /**
     * @var string
     *
     * @ORM\Column(name="code_postal", type="string", length=255)
     */
    protected $codePostal;

    /**
     * @var string
     *
     * @ORM\Column(name="status", type="string", length=45)
     */
    protected $statut;

    /**
     * @var int
     *
     * @ORM\Column(name="duree", type="integer")
     */
     private $duree;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_expiration", type="date", nullable=true)
     */
    protected $dateExpiration;

    /**
     * @var string
     *
     * @ORM\Column(name="telephone", type="string", length=255, nullable=true)
     */
    protected $telephone;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=255, nullable=true)
     */
    protected $email;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_validation", type="datetime", nullable=true)
     */
    protected $dateValidation;

    /**
     * @var string
     *
     * @ORM\Column(name="message_admin", type="string", length=255, nullable=true)
     */
    protected $messageAdmin;



    /**
     * Constructor
     */
    public function __construct()
    {
        $this->images = new \Doctrine\Common\Collections\ArrayCollection();
    }
    /**
     *
     * @return string
     */
    public function __toString()
    {
       return $this->titre != '' ? $this->titre : '';
    }

    public function getType()
    {
      return (new \ReflectionClass($this))->getShortName();
    }
    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set titre
     *
     * @param string $titre
     *
     * @return Annonce
     */
    public function setTitre($titre)
    {
        $this->titre = $titre;

        return $this;
    }

    /**
     * Get titre
     *
     * @return string
     */
    public function getTitre()
    {
        return $this->titre;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Annonce
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set codePostal
     *
     * @param string $codePostal
     *
     * @return Annonce
     */
    public function setCodePostal($codePostal)
    {
        $this->codePostal = $codePostal;

        return $this;
    }

    /**
     * Get codePostal
     *
     * @return string
     */
    public function getCodePostal()
    {
        return $this->codePostal;
    }

    /**
     * Set statut
     *
     * @param string $statut
     *
     * @return Annonce
     */
    public function setStatut($statut)
    {
        $this->statut = $statut;

        return $this;
    }

    /**
     * Get statut
     *
     * @return string
     */
    public function getStatut()
    {
        return $this->statut;
    }

    /**
     * Set dateExpiration
     *
     * @param \DateTime $dateExpiration
     *
     * @return Annonce
     */
    public function setDateExpiration($dateExpiration)
    {
        $this->dateExpiration = $dateExpiration;

        return $this;
    }

    /**
     * Get dateExpiration
     *
     * @return \DateTime
     */
    public function getDateExpiration()
    {
        return $this->dateExpiration;
    }

    /**
     * Set telephone
     *
     * @param string $telephone
     *
     * @return Annonce
     */
    public function setTelephone($telephone)
    {
        $this->telephone = $telephone;

        return $this;
    }

    /**
     * Get telephone
     *
     * @return string
     */
    public function getTelephone()
    {
        return $this->telephone;
    }

    /**
     * Set email
     *
     * @param string $email
     *
     * @return Annonce
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set dateValidation
     *
     * @param \DateTime $dateValidation
     *
     * @return Annonce
     */
    public function setDateValidation($dateValidation)
    {
        $this->dateValidation = $dateValidation;

        return $this;
    }

    /**
     * Get dateValidation
     *
     * @return \DateTime
     */
    public function getDateValidation()
    {
        return $this->dateValidation;
    }

    /**
     * Set messageAdmin
     *
     * @param string $messageAdmin
     *
     * @return Annonce
     */
    public function setMessageAdmin($messageAdmin)
    {
        $this->messageAdmin = $messageAdmin;

        return $this;
    }

    /**
     * Get messageAdmin
     *
     * @return string
     */
    public function getMessageAdmin()
    {
        return $this->messageAdmin;
    }

    /**
     * Add image
     *
     * @param \AnnonceBundle\Entity\AnnonceImage $image
     *
     * @return Annonce
     */
    public function addImage(\AnnonceBundle\Entity\AnnonceImage $image)
    {
        $this->images[] = $image;

        return $this;
    }

    /**
     * Remove image
     *
     * @param \AnnonceBundle\Entity\AnnonceImage $image
     */
    public function removeImage(\AnnonceBundle\Entity\AnnonceImage $image)
    {
        $this->images->removeElement($image);
    }

    /**
     * Get images
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getImages()
    {
        return $this->images;
    }

    /**
     * Set auteur
     *
     * @param \Application\Sonata\UserBundle\Entity\User $auteur
     *
     * @return Annonce
     */
    public function setAuteur(\Application\Sonata\UserBundle\Entity\User $auteur = null)
    {
        $this->auteur = $auteur;

        return $this;
    }

    /**
     * Get auteur
     *
     * @return \Application\Sonata\UserBundle\Entity\User
     */
    public function getAuteur()
    {
        return $this->auteur;
    }

    /**
     * Set duree
     *
     * @param integer $duree
     *
     * @return Annonce
     */
    public function setDuree($duree)
    {
        $this->duree = $duree;

        return $this;
    }

    /**
     * Get duree
     *
     * @return integer
     */
    public function getDuree()
    {
        return $this->duree;
    }
}
