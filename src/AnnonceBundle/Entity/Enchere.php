<?php

namespace AnnonceBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\Timestampable\Traits\TimestampableEntity;

/**
 * Enchere
 *
 * @ORM\Table(name="enchere")
 * @ORM\Entity(repositoryClass="AnnonceBundle\Repository\EnchereRepository")
 */
class Enchere
{
    use TimestampableEntity;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="montant", type="integer")
     */
    private $montant;

    /**
      * @var User
      *
      * @Gedmo\Blameable(on="create")
      * @ORM\ManyToOne(targetEntity="Application\Sonata\UserBundle\Entity\User")
      * many to one to allow encherisseur to make more than one enchere
      */
     private $encherisseur;

     /**
      * @var Offre
      *
      * @ORM\ManyToOne(targetEntity="Offre")
      */
     private $offre;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set montant
     *
     * @param integer $montant
     *
     * @return Enchere
     */
    public function setMontant($montant)
    {
        $this->montant = $montant;

        return $this;
    }

    /**
     * Get montant
     *
     * @return int
     */
    public function getMontant()
    {
        return $this->montant;
    }

    /**
     * Set encherisseur
     *
     * @param \Application\Sonata\UserBundle\Entity\User $encherisseur
     *
     * @return Enchere
     */
    public function setEncherisseur(\Application\Sonata\UserBundle\Entity\User $encherisseur = null)
    {
        $this->encherisseur = $encherisseur;

        return $this;
    }

    /**
     * Get encherisseur
     *
     * @return \Application\Sonata\UserBundle\Entity\User
     */
    public function getEncherisseur()
    {
        return $this->encherisseur;
    }

    /**
     * Set offre
     *
     * @param \AnnonceBundle\Entity\Offre $offre
     *
     * @return Enchere
     */
    public function setOffre(\AnnonceBundle\Entity\Offre $offre = null)
    {
        $this->offre = $offre;

        return $this;
    }

    /**
     * Get offre
     *
     * @return \AnnonceBundle\Entity\Offre
     */
    public function getOffre()
    {
        return $this->offre;
    }
}
