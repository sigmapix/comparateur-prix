<?php

namespace AnnonceBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * OffreEmploi
 *
 * @ORM\Table(name="offre_emploi")
 * @ORM\Entity(repositoryClass="AnnonceBundle\Repository\OffreEmploiRepository")
 */
class OffreEmploi extends Annonce
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var CategoryEmploi
     * @ORM\ManyToOne(targetEntity="CategorieEmploi", inversedBy="offresEmploi")
     * @ORM\JoinColumn(name="categorie_emploi_id", referencedColumnName="id")
     */
    private $categorieEmploi;

    /**
     * @var int
     *
     * @ORM\Column(name="type_contrat", type="integer")
     */
    private $typeContrat;

    /**
     * @var string
     *
     * @ORM\Column(name="salaire", type="string", length=255)
     */
    private $salaire;



    /**
     * Set typeContrat
     *
     * @param integer $typeContrat
     *
     * @return OffreEmploi
     */
    public function setTypeContrat($typeContrat)
    {
        $this->typeContrat = $typeContrat;

        return $this;
    }

    /**
     * Get typeContrat
     *
     * @return integer
     */
    public function getTypeContrat()
    {
        return $this->typeContrat;
    }

    /**
     * Set salaire
     *
     * @param string $salaire
     *
     * @return OffreEmploi
     */
    public function setSalaire($salaire)
    {
        $this->salaire = $salaire;

        return $this;
    }

    /**
     * Get salaire
     *
     * @return string
     */
    public function getSalaire()
    {
        return $this->salaire;
    }

    /**
     * Set categorieEmploi
     *
     * @param \AnnonceBundle\Entity\CategorieEmploi $categorieEmploi
     *
     * @return OffreEmploi
     */
    public function setCategorieEmploi(\AnnonceBundle\Entity\CategorieEmploi $categorieEmploi = null)
    {
        $this->categorieEmploi = $categorieEmploi;

        return $this;
    }

    /**
     * Get categorieEmploi
     *
     * @return \AnnonceBundle\Entity\CategorieEmploi
     */
    public function getCategorieEmploi()
    {
        return $this->categorieEmploi;
    }
}
