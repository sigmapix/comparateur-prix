<?php

namespace AnnonceBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use Symfony\Component\HttpFoundation\File\File;

/**
 * AnnonceImage
 *
 * @Vich\Uploadable
 *
 * @ORM\Table(name="annonce_image")
 * @ORM\Entity(repositoryClass="AnnonceBundle\Repository\AnnonceImageRepository")
 */
class AnnonceImage
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;


    /**
     * @Vich\UploadableField(mapping="annonce_image", fileNameProperty="imageName")
     *
     * @var File
     */
    private $image;

    /**
     * @ORM\Column(type="string", length=255)
     *
     * @var string
     */
    private $imageName;

    /**
      * @var Annonce
      *
      * @ORM\ManyToOne(targetEntity="AnnonceBundle\Entity\Annonce", inversedBy="images",cascade={"persist"})
      * @ORM\JoinColumn(nullable=false)
      */
    private $annonces;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set annonces
     *
     * @param \AnnonceBundle\Entity\Annonce $annonces
     *
     * @return AnnonceImage
     */
    public function setAnnonces(\AnnonceBundle\Entity\Annonce $annonces = null)
    {
        $this->annonces = $annonces;

        return $this;
    }

    /**
     * Get annonces
     *
     * @return \AnnonceBundle\Entity\Annonce
     */
    public function getAnnonces()
    {
        return $this->annonces;
    }

    /**
     *
     * @param File|\Symfony\Component\HttpFoundation\File\UploadedFile $image
     *
     * @return Annonce
     */
    public function setImage(File $image = null)
    {
        $this->image = $image;

        return $this;
    }

    /**
     * @return File|null
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * @param string $imageName
     *
     * @return Annonce
     */
    public function setImageName($imageName)
    {
        $this->imageName = $imageName;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getImageName()
    {
        return $this->imageName;
    }

}
