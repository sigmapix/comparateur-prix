<?php

namespace AnnonceBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Offre
 *
 * @ORM\Table(name="offre")
 * @ORM\Entity(repositoryClass="AnnonceBundle\Repository\OffreRepository")
 */
class Offre extends Annonce
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="frais", type="integer", nullable=true)
     */
    private $frais;

    /**
     * @var int
     *
     * @ORM\Column(name="prix", type="integer")
     */
    private $prix;

    /**
     * @var bool
     *
     * @ORM\Column(name="enchere", type="boolean")
     */
    private $enchere;

    /**
     * @var int
     *
     * @ORM\Column(name="prix_reserve", type="integer", nullable=true)
     */
    private $prixReserve;

    /**
     * @var CategoryPro
     * @ORM\ManyToOne(targetEntity="CategoriePro", inversedBy="offres")
     * @ORM\JoinColumn(name="categorie_pro_id", referencedColumnName="id")
     */
    private $categoriePro;


    /**
     * Set frais
     *
     * @param integer $frais
     *
     * @return Offre
     */
    public function setFrais($frais)
    {
        $this->frais = $frais;

        return $this;
    }

    /**
     * Get frais
     *
     * @return integer
     */
    public function getFrais()
    {
        return $this->frais;
    }

    /**
     * Set prix
     *
     * @param integer $prix
     *
     * @return Offre
     */
    public function setPrix($prix)
    {
        $this->prix = $prix;

        return $this;
    }

    /**
     * Get prix
     *
     * @return integer
     */
    public function getPrix()
    {
        return $this->prix;
    }

    /**
     * Set enchere
     *
     * @param boolean $enchere
     *
     * @return Offre
     */
    public function setEnchere($enchere)
    {
        $this->enchere = $enchere;

        return $this;
    }

    /**
     * Get enchere
     *
     * @return boolean
     */
    public function getEnchere()
    {
        return $this->enchere;
    }

    /**
     * Set prixReserve
     *
     * @param integer $prixReserve
     *
     * @return Offre
     */
    public function setPrixReserve($prixReserve)
    {
        $this->prixReserve = $prixReserve;

        return $this;
    }

    /**
     * Get prixReserve
     *
     * @return integer
     */
    public function getPrixReserve()
    {
        return $this->prixReserve;
    }

    /**
     * Set categoriePro
     *
     * @param \AnnonceBundle\Entity\CategoriePro $categoriePro
     *
     * @return Offre
     */
    public function setCategoriePro(\AnnonceBundle\Entity\CategoriePro $categoriePro = null)
    {
        $this->categoriePro = $categoriePro;

        return $this;
    }

    /**
     * Get categoriePro
     *
     * @return \AnnonceBundle\Entity\CategoriePro
     */
    public function getCategoriePro()
    {
        return $this->categoriePro;
    }
}
