<?php

namespace AnnonceBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Demande
 *
 * @ORM\Table(name="demande")
 * @ORM\Entity(repositoryClass="AnnonceBundle\Repository\DemandeRepository")
 */
class Demande extends Annonce
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var CategoryPro
     *
     * @ORM\ManyToOne(targetEntity="CategoriePro", inversedBy="demandes")
     * @ORM\JoinColumn(name="categorie_pro_id", referencedColumnName="id")
     */
    private $categoriePro;



    /**
     * Set categoriePro
     *
     * @param \AnnonceBundle\Entity\CategoriePro $categoriePro
     *
     * @return Demande
     */
    public function setCategoriePro(\AnnonceBundle\Entity\CategoriePro $categoriePro = null)
    {
        $this->categoriePro = $categoriePro;

        return $this;
    }

    /**
     * Get categoriePro
     *
     * @return \AnnonceBundle\Entity\CategoriePro
     */
    public function getCategoriePro()
    {
        return $this->categoriePro;
    }
}
