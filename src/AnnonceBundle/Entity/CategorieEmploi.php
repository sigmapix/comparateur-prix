<?php

namespace AnnonceBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Sonata\ClassificationBundle\Entity\BaseCategory as BaseCategory;

/**
 * Category
 *
 * @ORM\Table(name="category_emploi")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\CategoryRepository")
 */
class CategorieEmploi extends BaseCategory
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\OneToMany(targetEntity="OffreEmploi", mappedBy="categorieEmploi")
     * @ORM\JoinColumn(nullable=true)
     */
    private $offresEmploi;

    /**
     * @ORM\OneToMany(targetEntity="DemandeEmploi", mappedBy="categorieEmploi")
     * @ORM\JoinColumn(nullable=true)
     */
    private $demandesEmploi;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->offresEmploi = new \Doctrine\Common\Collections\ArrayCollection();
        $this->demandesEmploi = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * @param mixed $object
     * @return string
     *
     */
    public function toString($object)
    {
        return $object instanceof CategorieEmploi
            ? $object->getNom()
            : 'Categorie'; // shown in the breadcrumb on the create view
    }

    /**
     * Add offresEmploi
     *
     * @param \AnnonceBundle\Entity\OffreEmploi $offresEmploi
     *
     * @return CategorieEmploi
     */
    public function addOffresEmploi(\AnnonceBundle\Entity\OffreEmploi $offresEmploi)
    {
        $this->offresEmploi[] = $offresEmploi;

        return $this;
    }

    /**
     * Remove offresEmploi
     *
     * @param \AnnonceBundle\Entity\OffreEmploi $offresEmploi
     */
    public function removeOffresEmploi(\AnnonceBundle\Entity\OffreEmploi $offresEmploi)
    {
        $this->offresEmploi->removeElement($offresEmploi);
    }

    /**
     * Get offresEmploi
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getOffresEmploi()
    {
        return $this->offresEmploi;
    }

    /**
     * Add demandesEmploi
     *
     * @param \AnnonceBundle\Entity\DemandeEmploi $demandesEmploi
     *
     * @return CategorieEmploi
     */
    public function addDemandesEmploi(\AnnonceBundle\Entity\DemandeEmploi $demandesEmploi)
    {
        $this->demandesEmploi[] = $demandesEmploi;

        return $this;
    }

    /**
     * Remove demandesEmploi
     *
     * @param \AnnonceBundle\Entity\DemandeEmploi $demandesEmploi
     */
    public function removeDemandesEmploi(\AnnonceBundle\Entity\DemandeEmploi $demandesEmploi)
    {
        $this->demandesEmploi->removeElement($demandesEmploi);
    }

    /**
     * Get demandesEmploi
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getDemandesEmploi()
    {
        return $this->demandesEmploi;
    }
}
