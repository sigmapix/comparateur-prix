<?php

namespace AnnonceBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Lexik\Bundle\FormFilterBundle\Filter\FilterBuilderUpdater;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\EventDispatcher\EventDispatcher;
use AnnonceBundle\Filter\CategoryProFilterType;
use AnnonceBundle\Filter\CategoryEmploiFilterType;
use Symfony\Component\HttpFoundation\Request;
use AnnonceBundle\Entity\CategoriePro;
use AnnonceBundle\Entity\CategorieEmploi;
use AnnonceBundle\Entity\Offre;
use AnnonceBundle\Entity\Annonce;
use AnnonceBundle\Entity\AnnonceImage;
use AnnonceBundle\Entity\DemandeEmploiFichier;
use AnnonceBundle\Entity\Demande;
use AnnonceBundle\Entity\OffreEmploi;
use AnnonceBundle\Entity\DemandeEmploi;
use AnnonceBundle\Entity\Enchere;
use AnnonceBundle\Form\OffreType;
use AnnonceBundle\Form\OffreEmploiType;
use AnnonceBundle\Form\DemandeType;
use AnnonceBundle\Form\DemandeEmploiType;
use AnnonceBundle\Form\ReplyType;
use AnnonceBundle\Form\ReplyEnchereType;
use AnnonceBundle\Form\SearchType;
use AnnonceBundle\Manager\AnnonceManager;
use AnnonceBundle\Services\Mailer;
use AnnonceBundle\Event\ReplyReceivedEvent;
use Symfony\Component\Form\FormFactoryInterface;


class AnnonceController extends Controller
{

    /**
     * @Route("/annonces", name="annonces")
     */
    public function annoncesAction(Request $request, AnnonceManager $annonceManager)
    {
      //can't use Annonce::class directly since it give "AnnonceBundle/Entity/Annonce"
      //and this give rise to inconsistent up the URL
      return $this->render('AnnonceBundle:Annonce:annonce.html.twig', array(
        'offre' => new Offre(),
        'demande' => new Demande(),
        'demandeEmploi' => new DemandeEmploi(),
        'offreEmploi' => new OffreEmploi()
      ));
    }

    /**
     * @Route("/annonces/{type}",
     *  requirements={
     *         "type": "Demande|DemandeEmploi|Offre|OffreEmploi"
     *     },
     *  name="annonce")
     */
    public function indexAction(Request $request, AnnonceManager $annonceManager, FilterBuilderUpdater $filterLexik, FormFactoryInterface $formFactory, $type)
    {
        $annonceType = $this->getFullPath($type);
        $annonce = new $annonceType();

        $form = $this->getFormWithFilter($annonce, $formFactory);
        $formSearch = $this->getSearchForm();
        $repository = $annonceManager->getRepository($annonceType);

        if ($request->query->has($form->getName())) {
            $form->submit($request->query->get($form->getName()));
            $annonces = $repository->getByCategoryFilter($filterLexik, $form);
        }
        else {
            $annonces = $repository->findby(
              array('statut' => Annonce::STATUT_EN_COURS),
              array('dateValidation' => 'desc')
            );
        }

        // Si la requête est en POST pour le search
        if ($request->isMethod('POST')) {
            $formSearch->handleRequest($request);
            $searchTerm = $request->get('search')['search'];
            if($searchTerm !== null){
                $annonces = $repository->getBySearchTerm($searchTerm);
            }

        }

        return $this->render('AnnonceBundle:Annonce:index.html.twig', array(
            'form' => $form->createView(),
            'formSearch' => $formSearch->createView(),
            'type' => $type,
            'annonces' => $annonces,
        ));
    }

    /**
     * @Route("/annonce/{id}", name="annonce_show",
     *  requirements={"id" = "\d+"}
     * )
     * @ParamConverter("annonce", class="AnnonceBundle:Annonce")
     */
    public function showAction(Request $request, Annonce $annonce = null)
    {
      //check if annonce is null
      //if NOT null, then we check if status is en_cours
      //if NOT en_cours, we check if the annonce belongs to the user
      if($annonce === null || ($annonce->getStatut() !== Annonce::STATUT_EN_COURS && $this->getUser() !== $annonce->getAuteur())){
        return $this->render('AnnonceBundle:Annonce:error.html.twig', array());
      }
      $form = $this->getReplyForm($annonce);

      return $this->render('AnnonceBundle:Annonce:show.html.twig', array(
          'annonce' => $annonce,
          'form' => $form->createView(),
      ));

    }

    /**
     * @Route("/annonce/creation/{type}", name="creation_annonce")
     */
    public function createAction(Request $request, AnnonceManager $annonceManager, $type)
    {
      $annonceType = $this->getFullPath($type);
      $annonce = new $annonceType();

      $form = $this->createAnnonceFormByType($annonce);

      // Si la requête est en POST
      if ($request->isMethod('POST')) {
        //initilize fichier to null which it is unless of type DemandeEmploi
        $fichiers =null;

        //retrieve images from request
        //remove it from request object so that it passes the handleRequest
        //it fails the handleRequest function since the entity has AnnonceImage
        //and not file instances directly
        if($annonce instanceof Demande){

          $images = $request->files->get('demande')['images'];
          $request->files->remove('demande');
        }

        if($annonce instanceof DemandeEmploi){

          $images = $request->files->get('demande_emploi')['images'];
          $fichiers = $request->files->get('demande_emploi')['fichiers'];
          $request->files->remove('demande_emploi');
        }

        if($annonce instanceof Offre){

          $images = $request->files->get('offre')['images'];
          $request->files->remove('offre');
        }

        if($annonce instanceof   OffreEmploi){

          $images = $request->files->get('offre_emploi')['images'];
          $request->files->remove('offre_emploi');
        }

        $form->handleRequest($request);

        if ($form->isValid()) {

          $user = $this->getUser();
          $annonceManager->persistAnnonce($user, $request, $annonce, $images, $fichiers);
          $this->addFlash('notice', 'Annonce bien enregistrée.');
          return $this->redirectToRoute('mes_annonces');
        }
      }

      return $this->render('AnnonceBundle:Annonce:create.html.twig', array(
        'form' => $form->createView(),
      ));
    }



    /**
     * @Route("/annonce_sauvegarder/{id}", name="annonce_sauvegarder",
     *  requirements={"id" = "\d+"}
     * )
     * @ParamConverter("annonce", class="AnnonceBundle:Annonce")
     */
    public function sauvegardeAnnonceAction(Request $request, Annonce $annonce = null)
    {
      $user = $this->getUser();
      $userManager = $this->container->get('fos_user.user_manager');
      $user->addAnnonce($annonce);
      $userManager->updateUser($user);

      return $this->redirect($this->generateUrl('annonce_show',['id' => $annonce->getId()]));

    }

    /**
     * @Route("/mes_annonces", name="mes_annonces")
     */
    public function mesAnnoncesAction(Request $request, AnnonceManager $annonceManager)
    {
      $user = $this->getUser();
      if($user === null){
        return $this->redirectToRoute('fos_user_security_login');
      }
      $annonces = $annonceManager->getMesAnnonces(Annonce::class,$user);
      $annoncesSauvegardees = $user->getAnnonces();

      return $this->render('AnnonceBundle:Annonce:mesAnnonces.html.twig', array(
          'annonces' => $annonces,
          'annoncesSauvegardees' => $annoncesSauvegardees
      ));

    }


    /**
     * @Route("/annonce_modifier/{id}", name="annonce_modifier",
     *  requirements={"id" = "\d+"}
     * )
     * @ParamConverter("annonce", class="AnnonceBundle:Annonce")
     */
    public function modifierAnnonceAction(Request $request, AnnonceManager $annonceManager, Annonce $annonce = null)
    {

      //edit affichage of duree when editing
      $today = new \DateTime();
      $durationLeftInDays = $annonce->getDateExpiration()->diff($today)->days;
      $annonce->setDuree($durationLeftInDays+1);

      $form = $this->createAnnonceFormByType($annonce);

      // Si la requête est en POST
      if ($request->isMethod('POST')) {

        $fichiers =null;

        if($annonce instanceof Demande){
          $images = $request->files->get('demande')['images'];
          $request->files->remove('demande');
        }

        if($annonce instanceof DemandeEmploi){
          $images = $request->files->get('demande_emploi')['images'];
          $fichiers = $request->files->get('demande_emploi')['fichiers'];
          $request->files->remove('demande_emploi');
        }

        if($annonce instanceof Offre){
          $images = $request->files->get('offre')['images'];
          $request->files->remove('offre');
        }

        if($annonce instanceof OffreEmploi){
          $images = $request->files->get('offre_emploi')['images'];
          $request->files->remove('offre_emploi');
        }

        //delete images if required
        foreach($annonce->getImages() as $annonceImage){
          $name = 'image'. $annonceImage->getId();
          if($request->get($name) ===  "1"){
            $annonce->removeImage($annonceImage);
            $annonceManger->removeEntity($annonceImage);
            $annonceManger->persistAndFlush($annonce,true);
          }
        }

        //delete files if required
        if($annonce instanceof DemandeEmploi){
          foreach($annonce->getFichiers() as $annonceFichier){
            $name = 'fichier'. $annonceFichier->getId();
            if($request->get($name) ===  "1"){
              $annonce->removeFichier($annonceFichier);
              $annonceManger->removeEntity($annonceFichier);
              $annonceManger->persistAndFlush($annonce,true);
            }
          }
        }


        $annonce = $this->customHandleRequest($annonce, $request,$annonceManager);

        $user = $this->getUser();
        $annonceManager->persistAnnonce($user, $request, $annonce, $images, $fichiers);
        $this->addFlash('notice', 'Annonce bien enregistrée.');
        return $this->redirectToRoute('mes_annonces');
      }


      return $this->render('AnnonceBundle:Annonce:edit.html.twig', array(
        'form' => $form->createView(),
        'annonce' => $annonce
      ));
    }

    /**
     * @Route("/annonce_arreter/{id}", name="annonce_arreter",
     *  requirements={"id" = "\d+"}
     * )
     * @ParamConverter("annonce", class="AnnonceBundle:Annonce")
     */
    public function arreteAnnonceAction(Request $request, AnnonceManager $annonceManager, Annonce $annonce = null)
    {
      $workflow = $this->container->get('workflow.annonce_publishing');
      $workflow->apply($annonce, Annonce::STATUT_ARRETEE);
      $annonceManger->persistAndFlush($annonce,true);

      return $this->redirectToRoute('mes_annonces');
    }

    /**
     * @Route("/annonce_republier/{id}", name="annonce_republier",
     *  requirements={"id" = "\d+"}
     * )
     * @ParamConverter("annonce", class="AnnonceBundle:Annonce")
     */
    public function republieAnnonceAction(Request $request, AnnonceManager $annonceManager, Annonce $annonce = null)
    {
      $workflow = $this->container->get('workflow.annonce_publishing');
      $status= $annonce->getStatut();
      if($status === Annonce::STATUT_EXPIREE){
        $workflow->apply($annonce, 'brouillonC');
      }
      if($status === Annonce::STATUT_ARRETEE){
        $workflow->apply($annonce, 'brouillonB');
      }
      if($status === Annonce::STATUT_REFUSEE){
        $workflow->apply($annonce, 'brouillonA');
      }
      $annonce->setDateValidation(null);
      $annonceManger->persistAndFlush($annonce,true);

      return $this->redirectToRoute('mes_annonces');
    }


    /**
     * @Route("/annonce_reponse/{id}", name="annonce_reponse",
     *  requirements={"id" = "\d+"}
     * )
     * @ParamConverter("annonce", class="AnnonceBundle:Annonce")
     */
    public function repondreAnnonceAction(Request $request, AnnonceManager $annonceManager, Annonce $annonce = null, Mailer $mailer)
    {
      $user = $this->getUser();

      if($annonce instanceof Offre && $annonce->getEnchere()){
        $form = $request -> get('reply_enchere');
        $annonceManager->persistEnchere($user, $annonce, $form);
        $this->addFlash('notice', 'Annonce bien enregistrée.');
        return $this->redirect($this->generateUrl('annonce_show',['id' => $annonce->getId()]));
      }

      $event = new ReplyReceivedEvent($request, $annonce, $user);
      $dispatcher = $this->get('event_dispatcher');
      $dispatcher->dispatch(ReplyReceivedEvent::NAME, $event);

      $this->addFlash('notice', 'Reponse bien enregistrée.');
      return $this->redirect($this->generateUrl('annonce_show',['id' => $annonce->getId()]));
    }

    private function getFormWithFilter($annonce, FormFactoryInterface $formFactory)
    {
      if($annonce instanceof Offre || $annonce instanceof Demande){
          $form = $formFactory->create(CategoryProFilterType::class);
      }

      if($annonce instanceof OffreEmploi || $annonce instanceof DemandeEmploi){
          $form = $formFactory->create(CategoryEmploiFilterType::class);
      }

      return $form;
    }


    private function getReplyForm($annonce)
    {
      if($annonce instanceof Offre && $annonce->getEnchere()){

          return $form = $this->createForm(ReplyEnchereType::class);
      }

      return $form = $this->createForm(ReplyType::class);
    }

    private function getSearchForm()
    {
      return $form = $this->createForm(SearchType::class);
    }

    public function createAnnonceFormByType($annonce)
    {
        if($annonce instanceof Offre){
            $form = $this->createForm(OffreType::class, $annonce);
        }

        if($annonce instanceof OffreEmploi){
            $form = $this->createForm(OffreEmploiType::class, $annonce);
        }

        if($annonce instanceof Demande){
            $form = $this->createForm(DemandeType::class, $annonce);
        }

        if($annonce instanceof DemandeEmploi){
            $form = $this->createForm(DemandeEmploiType::class, $annonce);
        }

        return $form;
    }

    private function customHandleRequest($annonce, $request,$annonceManager){
      $repositoryPro = $annonceManager->getRepository(CategoriePro::class);
      $repositoryEmploi = $annonceManager->getRepository(CategorieEmploi::class);
      if($annonce instanceof Offre){
          $variables = $request->get('offre');
          $category = $repositoryPro->find($variables['categoriePro']);
          $annonce->setCategoriePro($category);
          $annonce->setFrais($variables['frais']);
          $annonce->setPrix($variables['prix']);
          if(isset($variables['enchere'])){
            $annonce->setEnchere($variables['enchere']);
            $annonce->setPrixReserve($variables['prixReserve']);
          }
      }

      if($annonce instanceof OffreEmploi){
          $variables = $request->get('offre_emploi');
          $category = $repositoryEmploi->find($variables['categorieEmploi']);
          $annonce->setCategorieEmploi($category);
          $annonce->setTypeContrat($variables['typeContrat']);
          $annonce->setSalaire($variables['salaire']);
      }

      if($annonce instanceof Demande){
          $variables = $request->get('demande');
          $category = $repositoryPro->find($variables['categoriePro']);
          $annonce->setCategoriePro($category);
      }

      if($annonce instanceof DemandeEmploi){
          $variables = $request->get('demande_emploi');
          $category = $repositoryEmploi->find($variables['categorieEmploi']);
          $annonce->setCategorieEmploi($category);
      }


      $annonce->setTitre($variables['titre']);
      $annonce->setDescription($variables['description']);
      $annonce->setCodePostal($variables['codePostal']);
      $annonce->setEmail($variables['email']);
      $annonce->setTelephone($variables['telephone']);
      $annonce->setDuree($variables['duree']);

      return $annonce;

    }

    //since we have only the short name,
    //we cannot instantiate directly
    //we retrieve the base path from the class Annonce
    //then we change "Annonce" to the type that we have
    // ASSUMPTION: all entities are in the same folder as Annonce.
    private function getFullPath($type)
    {
      $basePath = Annonce::class;
      $basePathArray = explode('\\', $basePath);
      $basePathArray[2] =  $type;
      $annonceType = implode('\\', $basePathArray);

      return $annonceType;
    }
}
