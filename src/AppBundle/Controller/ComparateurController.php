<?php

namespace AppBundle\Controller;

use AppBundle\Services\PriceUpdater;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity\Produit;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use AppBundle\Filter\CategoryFilterType;

/**
 * Class ComparateurController
 * @package AppBundle\Controller
 */
class ComparateurController extends Controller
{

    /**
     * @Route("/produits", name="produit")
     */
    public function indexAction(Request $request)
    {

      $form = $this->get('form.factory')->create(CategoryFilterType::class);
      $produits = null;

      if ($request->query->has($form->getName())) {
          // manually bind values from the request
          $form->submit($request->query->get($form->getName()));
          //$name = $request->query->get($form->getName())['categories'];

          // initialize a query builder
          $filterBuilder = $this->get('doctrine.orm.entity_manager')
              ->getRepository('AppBundle:Produit')
              ->createQueryBuilder('p');

          // build the query from the given form object
         $this->get('lexik_form_filter.query_builder_updater')->addFilterConditions($form, $filterBuilder);

          $produits = $filterBuilder->getQuery()->getResult();


      } else {
          $repository = $this->getDoctrine()
            ->getManager()
            ->getRepository('AppBundle:Produit')
          ;
          $produits = $repository->findAll();
      }

      return $this->render('AppBundle:Comparateur:produit.html.twig', array(
          'form' => $form->createView(),
          'produits' => $produits,
      ));

    }

    /**
     * @Route("/produit/{id}", name="produit_show",
     *  requirements={"id" = "\d+"}
     * )
     * @ParamConverter("produit", class="AppBundle:Produit")
     */
    public function showAction(Request $request, Produit $produit = null)
    {

      return $this->render('AppBundle:Comparateur:produit_show.html.twig', array(
          'produit' => $produit,
      ));

    }
}
