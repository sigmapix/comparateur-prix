<?php

namespace AppBundle\Controller;

use Sonata\AdminBundle\Controller\CRUDController as Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use AppBundle\Services\PriceUpdater;
use Symfony\Component\HttpFoundation\RedirectResponse;

class CRUDController extends Controller
{
    public function updatePriceAction()
    {
      $produit = $this->admin->getSubject();
      if($produit === null){

          $this->addFlash("notice", $this->trans('message_error',[],'messages'));

          return new RedirectResponse($this->admin->generateUrl('list'));
      }

      $priceUpdater = $this->container->get(PriceUpdater::SERVICE_NAME);

      foreach ($produit->getProduitHasBoutique() as $produitHasBoutique){
          $priceUpdater->updatePrice($produitHasBoutique);
      }

      $this->addFlash("success", $this->trans('message_price_update_success',[],'messages'));

      return new RedirectResponse($this->admin->generateUrl('list'));
    }
}
