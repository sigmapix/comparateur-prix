<?php
namespace AppBundle\Filter;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Lexik\Bundle\FormFilterBundle\Filter\Form\Type as Filters;

/**
 * Class CategoryFilterType
 * @package AppBundle\Filter
 */
class CategoryFilterType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
        ->add('categorie', Filters\EntityFilterType::class, array(
                'class'         => 'AppBundle:Category',
                'expanded'      => false,
                'multiple'      => false,
                'placeholder'   => 'Tous',
                'query_builder' => function ($repository) {
                  return $repository->createQueryBuilder('c')
                                    ->select('c')
                                    ->where('c.enabled = 1'); },
                'apply_filter'  => function ( $filterQuery, $field, $value)
                {
                  if($value['value'] !== null){
                    $query = $filterQuery->getQueryBuilder();
                    $query->innerJoin('p.categories','c');
                    $query->where($query->expr()->in('c.id', $value['value']->getId()));
                  }
                },
            ));
    }

    public function getBlockPrefix()
    {
        return 'category_filter';
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'csrf_protection'   => false,
            'validation_groups' => array('filtering') // avoid NotBlank() constraint-related message
        ));
    }
}
