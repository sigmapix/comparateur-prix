<?php

namespace AppBundle\Entity;

use AppBundle\Traits\TimestampableTrait;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;


/**
 * Produit
 *
 * @ORM\Table(name="produit")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ProduitRepository")
 */
class Produit
{
    use TimestampableEntity;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nom", type="string", length=255)
     */
    private $nom;


    /**
    * @ORM\OneToMany(targetEntity="AppBundle\Entity\ProduitHasBoutique", mappedBy="produit" ,cascade={"persist", "remove"},orphanRemoval=true)
    */
    private $produitHasBoutique;

    /**
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\Category", )
     */
    private $categories;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nom
     *
     * @param string $nom
     *
     * @return Produit
     */
    public function setNom($nom)
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * Get nom
     *
     * @return string
     */
    public function getNom()
    {
        return $this->nom;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->produitHasBoutique = new \Doctrine\Common\Collections\ArrayCollection();
        $this->categories = new \Doctrine\Common\Collections\ArrayCollection();
    }

    public function __toString()
    {
        return $this->nom != '' ? $this->nom : '';
    }

    /**
     * Add produitHasBoutique
     *
     * @param \AppBundle\Entity\ProduitHasBoutique $produitHasBoutique
     *
     * @return Produit
     */
    public function addProduitHasBoutique(\AppBundle\Entity\ProduitHasBoutique $produitHasBoutique)
    {
        $this->produitHasBoutique[] = $produitHasBoutique;

        return $this;
    }

    /**
     * Remove produitHasBoutique
     *
     * @param \AppBundle\Entity\ProduitHasBoutique $produitHasBoutique
     */
    public function removeProduitHasBoutique(\AppBundle\Entity\ProduitHasBoutique $produitHasBoutique)
    {
        $this->produitHasBoutique->removeElement($produitHasBoutique);
    }

    /**
     * Get produitHasBoutique
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getProduitHasBoutique()
    {
        return $this->produitHasBoutique;
    }

    /**
     * Add category
     *
     * @param \AppBundle\Entity\Category $category
     *
     * @return Produit
     */
    public function addCategory(\AppBundle\Entity\Category $category)
    {
        $this->categories[] = $category;

        return $this;
    }

    /**
     * Remove category
     *
     * @param \AppBundle\Entity\Category $category
     */
    public function removeCategory(\AppBundle\Entity\Category $category)
    {
        $this->categories->removeElement($category);
    }

    /**
     * Get categories
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCategories()
    {
        return $this->categories;
    }
}
