<?php

namespace AppBundle\Entity;

use AppBundle\Traits\TimestampableTrait;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Symfony\Component\Validator\Context\ExecutionContextInterface;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * ProduitHasBoutique
 *
 * @ORM\Table(name="produit_has_boutique")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ProduitHasBoutiqueRepository")
 */
class ProduitHasBoutique
{
    use TimestampableEntity;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="url", type="string", length=255)
     *
     * @Assert\Url(
     *    message = "The url '{{ value }}' is not a valid url",
     * )
     */
    private $url;

    /**
     * @var string
     *
     * @ORM\Column(name="css", type="string", length=255, nullable=true)
     */
    private $css;

    /**
     * @var string
     *
     * @ORM\Column(name="prix", type="decimal", precision=10, scale=2, nullable=true)
     */
    private $prix;

    /**
     * @var string
     *
     * @ORM\Column(name="prix_full_text", type="string", length=255, nullable=true)
     */
    private $prixFullText;

    /**
     * @var int
     *
     * @ORM\Column(name="statut", type="integer", nullable=true)
     */
    private $statut;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Boutique", inversedBy="produitHasBoutique",cascade={"persist"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $boutique;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Produit", inversedBy="produitHasBoutique")
     * @ORM\JoinColumn(nullable=false)
     */
    private $produit;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set url
     *
     * @param string $url
     *
     * @return ProduitHasBoutique
     */
    public function setUrl($url)
    {
        $this->url = $url;

        return $this;
    }

    /**
     * Get url
     *
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * Set css
     *
     * @param string $css
     *
     * @return ProduitHasBoutique
     */
    public function setCss($css)
    {
        $this->css = $css;

        return $this;
    }

    /**
     * Get css
     *
     * @return string
     */
    public function getCss()
    {
        return $this->css;
    }

    /**
     * Set prix
     *
     * @param string $prix
     *
     * @return ProduitHasBoutique
     */
    public function setPrix($prix)
    {
        $this->prix = $prix;

        return $this;
    }

    /**
     * Get prix
     *
     * @return string
     */
    public function getPrix()
    {
        return $this->prix;
    }

    /**
     * Set statut
     *
     * @param integer $statut
     *
     * @return ProduitHasBoutique
     */
    public function setStatut($statut)
    {
        $this->statut = $statut;

        return $this;
    }

    /**
     * Get statut
     *
     * @return int
     */
    public function getStatut()
    {
        return $this->statut;
    }


    /**
     * Set boutique
     *
     * @param \AppBundle\Entity\Boutique $boutique
     *
     * @return ProduitHasBoutique
     */
    public function setBoutique(\AppBundle\Entity\Boutique $boutique)
    {
        $this->boutique = $boutique;

        return $this;
    }

    /**
     * Get boutique
     *
     * @return \AppBundle\Entity\Boutique
     */
    public function getBoutique()
    {
        return $this->boutique;
    }

    /**
     * Set produit
     *
     * @param \AppBundle\Entity\Produit $produit
     *
     * @return ProduitHasBoutique
     */
    public function setProduit(\AppBundle\Entity\Produit $produit)
    {
        $this->produit = $produit;

        return $this;
    }

    /**
     * Get produit
     *
     * @return \AppBundle\Entity\Produit
     */
    public function getProduit()
    {
        return $this->produit;
    }

    /**
     * Set prixFullText
     *
     * @param string $prixFullText
     *
     * @return ProduitHasBoutique
     */
    public function setPrixFullText($prixFullText)
    {
        $this->prixFullText = $prixFullText;

        return $this;
    }

    /**
     * Get prixFullText
     *
     * @return string
     */
    public function getPrixFullText()
    {
        return $this->prixFullText;
    }

    public function __toString()
    {
        return $this->getProduit()->getNom() != '' ? $this->getProduit()->getNom() : '';
    }
}
