<?php

namespace AppBundle\Entity;

use AppBundle\Traits\TimestampableTrait;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;

/**
 * Boutique
 *
 * @ORM\Table(name="boutique")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\BoutiqueRepository")
 */
class Boutique
{
    use TimestampableEntity;
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nom", type="string", length=255, unique=true)
     */
    private $nom;

    /**
     * @var string
     *
     * @ORM\Column(name="css", type="string", length=255)
     */
    private $css;


    /**
    * @ORM\OneToMany(targetEntity="AppBundle\Entity\ProduitHasBoutique", mappedBy="boutique")
    */
    private $produitHasBoutique;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nom
     *
     * @param string $nom
     *
     * @return Boutique
     */
    public function setNom($nom)
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * Get nom
     *
     * @return string
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Set css
     *
     * @param string $css
     *
     * @return Boutique
     */
    public function setCss($css)
    {
        $this->css = $css;

        return $this;
    }

    /**
     * Get css
     *
     * @return string
     */
    public function getCss()
    {
        return $this->css;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->produitHasBoutique = new \Doctrine\Common\Collections\ArrayCollection();
    }

    public function __toString()
    {
        return $this->nom != '' ? $this->nom : '';
    }

    /**
     * Add produitHasBoutique
     *
     * @param \AppBundle\Entity\ProduitHasBoutique $produitHasBoutique
     *
     * @return Boutique
     */
    public function addProduitHasBoutique(\AppBundle\Entity\ProduitHasBoutique $produitHasBoutique)
    {
        $this->produitHasBoutique[] = $produitHasBoutique;

        return $this;
    }

    /**
     * Remove produitHasBoutique
     *
     * @param \AppBundle\Entity\ProduitHasBoutique $produitHasBoutique
     */
    public function removeProduitHasBoutique(\AppBundle\Entity\ProduitHasBoutique $produitHasBoutique)
    {
        $this->produitHasBoutique->removeElement($produitHasBoutique);
    }

    /**
     * Get produitHasBoutique
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getProduitHasBoutique()
    {
        return $this->produitHasBoutique;
    }
}
