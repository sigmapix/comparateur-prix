<?php

namespace AppBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

use Application\Sonata\UserBundle\Entity\User;
use Application\Sonata\UserBundle\Entity\Group;

class LoadUserData extends AbstractFixture implements OrderedFixtureInterface, ContainerAwareInterface
{
    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * {@inheritDoc}
     */
    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
        $factory = $this->container->get('security.encoder_factory');
        $this->loadAdmins($manager, $factory);
        $manager->flush();
    }

    public function loadAdmins(ObjectManager $manager, $factory)
    {
        $users = array(
            array('maxime','grfdg56', 'maxime@sigmapix.com', 'Maxime', 'Venin', 'Super Administrateurs'),
            array('vladimir','grfdg56', 'vladimir@sigmapix.com', 'Vladimir', 'Jeliazkov', 'Super Administrateurs'),
            array('alexandre','grfdg56', 'alexandre@sigmapix.com', 'Alexandre', 'Mootassem', 'Super Administrateurs'),
        );
        $groups = array(
            array('Super Administrateurs',array('ROLE_SUPER_ADMIN')),
            array('Administrateurs',array('ROLE_ADMIN'))
        );
        foreach ($groups as $value) {
            $group = new Group($value[0],$value[1]);
            $groupsTable[$value[0]] = $group;
            $manager->persist($group);
        }
        foreach ($users as $value) {
            $user = new User();
            $user->setRoles(['ROLE_ADMIN']);
            $user->setUsername($value[0]);
            $encoder = $factory->getEncoder($user);
            $user->setPlainPassword($value[1]);
            $user->setEmail($value[2]);
            $user->setFirstname($value[3]);
            $user->setLastname($value[4]);
            $user->addGroup($groupsTable[$value[5]]);
            $user->setEnabled(true);
            $user->setSalt("salt");
            $manager->persist($user);
        }

        $manager->flush();
    }

    /**
     * Get the order of this fixture
     *
     * @return integer
     */
    public function getOrder()
    {
        return 10;
    }
}
