<?php

namespace AppBundle\Command;

use AppBundle\Services\PriceUpdater;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class UpdatePriceAllProduit
 * @package AppBundle\Command
 */
class UpdatePriceAllProduit extends ContainerAwareCommand
{

    protected function configure()
    {
      $this
      // the name of the command (the part after "bin/console")
      ->setName('comparateur:mise_a_jour_produits')

      // the short description shown while running "php bin/console list"
      ->setDescription('Mis a jour de tous les prix des produits')

      ->addArgument('option', InputArgument::OPTIONAL, 'Quel option souhaitez vous lancer?')
      // the full command description shown when running the command with
      // the "--help" option
      ->setHelp('Cette commande vous permet de mettre a jour le prix de tous les produits');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $priceUpdater = $this->getContainer()->get(PriceUpdater::SERVICE_NAME);

        // On récupère le repository
        $repository = $this->getContainer()->get('doctrine')
          ->getManager()
          ->getRepository('AppBundle:Produit')
        ;

        // On récupère tous les produits
        $produits = $repository ->findAll();

        $option = $input->getArgument('option');

        //par default on lance la mise a jour de tous les produits
        if ($option === null){
            $output->writeln('Debut de la mise a jour des prix de tous les produits!');
            foreach($produits as $produit){
              $output->writeln('Debut de la mise a jour du produit: '.$produit->getNom());
              foreach ($produit->getProduitHasBoutique() as $produitHasBoutique){
                $priceUpdater -> updatePrice($produitHasBoutique);
              }
            }
            //
        } else if ($option === 'KO'){
            $output->writeln('Debut de la mise a jour des prix des produits KO!');
            foreach($produits as $produit){
              $output->writeln('Debut de la mise a jour du produit: '.$produit->getNom());
              foreach ($produit->getProduitHasBoutique() as $produitHasBoutique){
                if($produitHasBoutique->getStatut() != Response::HTTP_OK){
                  $priceUpdater -> updatePrice($produitHasBoutique);
                }
              }
            }
        } else {
            $output->writeln("L'option " . $option . " n'existe pas!");
        }


    }
}
