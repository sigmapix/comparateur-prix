<?php

namespace AppBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\CoreBundle\Validator\ErrorElement;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;

/**
 * Class CategoryAdmin
 * @package AppBundle\Admin
 */
class CategoryAdmin extends AbstractAdmin
{
    protected $translationDomain = 'SonataAdminBundle';

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper ->add('name',TextType::class,[
                        'label' => $this->trans('label_nom')
                    ])
                    ->add('enabled',CheckboxType::class,[
                        'required' => false,
                        'label' => $this->trans('label_actif')
                    ])
                    ->add('description',TextareaType::class,
                        ['label' => $this->trans('label_description')]
                    );
    }

    /**
     * @param ErrorElement $errorElement
     * @param mixed $object
     */
    public function validate(ErrorElement $errorElement, $object)
    {
        $errorElement
            ->with('name')
                ->assertLength(array('max' => 255))
            ->end()
        ;
    }

    /**
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper ->add('name',null,[
                            'label' => $this->trans('label_nom')
                          ])
                          ->add('enabled',null,[
                            'label' => $this->trans('label_actif')
                          ])
                          ->add('description',null,[
                            'label' => $this->trans('label_description')
                          ])
                          ;
    }

    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper ->addIdentifier('name',null,[
                      'label' => $this->trans('label_nom')
                    ])
                    ->addIdentifier('enabled',null,[
                      'label' => $this->trans('label_actif')
                    ])
                    ->addIdentifier('description',null,[
                      'label' => $this->trans('label_description')
                    ])
                    ;
    }

    /**
     * @param mixed $object
     * @return string
     *
     */
    public function toString($object)
    {
        return $object instanceof Category
            ? $object->getNom()
            : 'Categorie'; // shown in the breadcrumb on the create view
    }
}
