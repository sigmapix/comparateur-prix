<?php

namespace AppBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\CoreBundle\Validator\ErrorElement;
use Sonata\AdminBundle\Route\RouteCollection;
use Symfony\Component\Form\Extension\Core\Type\TextType;

/**
 * Class ProduitAdmin
 * @package AppBundle\Admin
 */
class ProduitAdmin extends AbstractAdmin
{
    protected $translationDomain = 'SonataAdminBundle';
    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $em = $this->modelManager->getEntityManager('AppBundle\Entity\Category');

        $query = $em->createQueryBuilder('c')
            ->select('c')
            ->from('AppBundle:Category', 'c')
            ->where('c.enabled = 1');

        $formMapper ->add('nom',TextType::class,[
                        'label' => $this->trans('label_nom')
                    ])
                    ->add('categories','sonata_type_model',[
                        'multiple' => true,
                        'property' => 'name',
                        'btn_add'  => false,
                        'query'    => $query,
                        'label' => $this->trans('label_categorie')
                    ])
                    ->add('produitHasBoutique', 'sonata_type_collection', array(
                        'label' => $this->trans('label_produitHasBoutique'),
                        'type_options' => array( 'delete' => true )), array(
                            'by_reference' => true,
                            'edit' => 'inline',
                            'inline' => 'table',
                            'required' => false
                    ));
    }



    /**
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper ->add('nom',null,[
                          'label' => $this->trans('label_nom')
                        ])
                        ->add('categories',null,[
                          'label' => $this->trans('label_categorie')
                        ])
                        ;
    }

    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper ->addIdentifier('nom',null,[
                      'label' => $this->trans('label_nom')
                    ])
                    ->addIdentifier('categories',null,[
                      'label' => $this->trans('label_categorie')
                    ])
                    ->add('_action', null, array(
                        'actions' => array(
                            'updatePrice' => array(
                                'template' => 'AppBundle:CRUD:update_price.html.twig'
                            )
                        )
                    ))
                    ;
    }
    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->add('updatePrice', $this->getRouterIdParameter());
    }

    /**
     * @param mixed $object
     */
    public function prePersist($object)
    {
        foreach ($object->getProduitHasBoutique() as $produitHasBoutique) {
            $produitHasBoutique->setProduit($object);
        }
    }

    /**
     * @param mixed $object
     */
    public function preUpdate($object)
    {
        foreach ($object->getProduitHasBoutique() as $produitHasBoutique) {
            $produitHasBoutique->setProduit($object);
        }
    }

    /**
     * @param mixed $object
     * @return string
     */
    public function toString($object)
    {
        return $object instanceof Produit
            ? $object->getNom()
            : 'Produit'; // shown in the breadcrumb on the create view
    }

    /**
     * @param ErrorElement $errorElement
     * @param mixed $object
     */
    public function validate(ErrorElement $errorElement, $object)
    {
        $errorElement
            ->with('nom')
                ->assertLength(array('max' => 255))
            ->end()
        ;
    }

}
