<?php

namespace AppBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\CoreBundle\Validator\ErrorElement;
use Symfony\Component\Form\Extension\Core\Type\TextType;

/**
 * Class BoutiqueAdmin
 * @package AppBundle\Admin
 */
class BoutiqueAdmin extends AbstractAdmin
{
    protected $translationDomain = 'SonataAdminBundle';
    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper ->add('nom',TextType::class,[
                        'label' => $this->trans('label_nom')
                    ])
                    ->add('css',TextType::class,[
                        'label' => $this->trans('label_css')
                    ])
                    ;
    }

    /**
     * @param ErrorElement $errorElement
     * @param mixed $object
     */
    public function validate(ErrorElement $errorElement, $object)
    {
        $errorElement
            ->with('nom')
                ->assertLength(array('max' => 255))
            ->end()
            ->with('css')
                ->assertLength(array('max' => 255))
            ->end()
        ;
    }

    /**
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper ->add('nom',null,[
                          'label' => $this->trans('label_nom')
                        ])
                        ->add('css',null,[
                          'label' => $this->trans('label_css')
                        ])
                        ;
    }

    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper ->addIdentifier('nom',null,[
                      'label' => $this->trans('label_nom')
                    ])
                    ->addIdentifier('css',null,[
                      'label' => $this->trans('label_css')
                    ])
                    ;
    }

    /**
     * @param mixed $object
     * @return string
     */
    public function toString($object)
    {
        return $object instanceof Boutique
            ? $object->getNom()
            : 'Boutique'; // shown in the breadcrumb on the create view
    }
}
