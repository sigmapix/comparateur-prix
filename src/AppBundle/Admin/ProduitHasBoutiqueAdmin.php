<?php

namespace AppBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;

/**
 * Class ProduitHasBoutiqueAdmin
 * @package AppBundle\Admin
 */
class ProduitHasBoutiqueAdmin extends AbstractAdmin
{
    protected $translationDomain = 'SonataAdminBundle';
    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper ->add('boutique',null,[
                        'label' => $this->trans('label_boutique')
                      ])
                    ->add('url',null,[
                        'label' => $this->trans('label_url')
                      ])
                    ->add('css',null,[
                        'label' => $this->trans('label_css')
                      ])
                    ->add('prix', null,array(
                        'disabled' => true,
                        'label' => $this->trans('label_prix')
                    ))
                    ->add('prixFullText', null,array(
                        'disabled' => true,
                        'label' => $this->trans('label_prix_full_text')
                    ))
                    ->add('statut', null,array(
                        'disabled' => true,
                        'label' => $this->trans('label_statut')
                    ))
                    ;
    }

    /**
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper ->add('boutique')
                        ->add('produit')
                        ->add('url')
                        ->add('css')
                        ->add('prix')
                        ->add('prixFullText')
                        ->add('statut');
    }

    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper ->addIdentifier('url')
                    ->addIdentifier('css')
                    ->addIdentifier('prix')
                    ->add('prixFullText')
                    ->addIdentifier('statut');
    }

}
