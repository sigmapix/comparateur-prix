<?php

namespace AppBundle\Services;

use Symfony\Component\DomCrawler\Crawler;
use Doctrine\ORM\EntityManager;
use AppBundle\Entity\ProduitHasBoutique;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class PriceUpdater
 * @package AppBundle\Services
 */
class PriceUpdater
{
    /**
     * Service name
     */
    const SERVICE_NAME = 'app.price_updater';
    /**
     * Custom code for css not found on page
     */
  const CSS_NOT_FOUND = 1000;
  const PRICE_KO = 2000;

    /**
     * @var EntityManager
     */
  protected $em;

    /**
     * PriceUpdater constructor.
     * @param EntityManager $entityManager
     */
  public function __construct(EntityManager $entityManager)
  {
      $this->em = $entityManager;
  }

    /**
     * @param ProduitHasBoutique $produitHasBoutique
     * @return bool
     */
  public function updatePrice(ProduitHasBoutique $produitHasBoutique){

    $url = $produitHasBoutique->getUrl();
    $cssProduit = $produitHasBoutique->getCss();
    $cssBoutique = $produitHasBoutique->getBoutique()->getCss();

    //set css to the one in ProduitHasBoutique
    //if not present, then we refer to the css in Boutique
    $css = $cssProduit;
    if($cssProduit === null ){
      $css = $cssBoutique;
    }

    //this returns the response code
    //we assign this variable to checkUrl
    $status = $this->checkUrl($url);

    //we proceed only if response code is 200
    //if the css is not found on the page, it throws an invalid argument exception
    if($status === Response::HTTP_OK){
        $html = file_get_contents($url);
        $crawler  = new Crawler($html);

        try{
            $priceFullText    = $crawler->filter($css)->text();
            $produitHasBoutique->setPrixFullText($priceFullText);
            $priceArray = explode('€',$priceFullText);
            $priceOnly = trim(str_replace(",",".","$priceArray[0]"));
            if(is_numeric($priceOnly)){
                $produitHasBoutique->setPrix($priceOnly);
            } else {
                $status = SELF::PRICE_KO;
            }
        } catch (\InvalidArgumentException $notFoundException){
            $status = SELF::CSS_NOT_FOUND;
        }
    }

    //we update the status of the product with the result of the operation.
    //if process is successful, we update status to 200
    //if process is not successul, the appropriate response code is recorded
    $produitHasBoutique->setStatut($status);

    $this->em->persist($produitHasBoutique);
    $this->em->flush();

    return TRUE;
  }

    /**
     * Perform a curl on the URL
     * @param $url
     * @return bool|mixed
     */
  private function checkUrl($url) {
	    if (!$url) { return 404; }

	    $curl_resource = curl_init($url);

	    curl_setopt($curl_resource, CURLOPT_RETURNTRANSFER, true);
	    curl_exec($curl_resource);

      $responseCode = curl_getinfo($curl_resource, CURLINFO_HTTP_CODE);

      return $responseCode;
	}

}
