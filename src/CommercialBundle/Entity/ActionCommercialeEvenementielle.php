<?php

namespace CommercialBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ActionCommercialeEvenementielle
 *
 * @ORM\Table(name="evenementielle")
 * @ORM\Entity(repositoryClass="CommercialBundle\Repository\ActionCommercialeEvenementielleRepository")
 */
class ActionCommercialeEvenementielle extends AbstractActionCommerciale
{
    /**
     *
     * @ORM\OneToMany(targetEntity="CommercialBundle\Entity\EtapeCommercialeEvenementielle", mappedBy="actionCommerciale", cascade={"persist", "remove"},orphanRemoval=true)
     * @ORM\OrderBy({"delaiJours" = "ASC"})
     */
    protected $etapeCommerciale;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->etapeCommerciale = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add etapeCommerciale
     *
     * @param \CommercialBundle\Entity\EtapeCommercialeEvenementielle $etapeCommerciale
     *
     * @return ActionCommercialeEvenementielle
     */
    public function addEtapeCommerciale(\CommercialBundle\Entity\EtapeCommercialeEvenementielle $etapeCommerciale)
    {
        $this->etapeCommerciale[] = $etapeCommerciale;

        return $this;
    }

    /**
     * Remove etapeCommerciale
     *
     * @param \CommercialBundle\Entity\EtapeCommercialeEvenementielle $etapeCommerciale
     */
    public function removeEtapeCommerciale(\CommercialBundle\Entity\EtapeCommercialeEvenementielle $etapeCommerciale)
    {
        $this->etapeCommerciale->removeElement($etapeCommerciale);
    }

    /**
     * Get etapeCommerciale
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getEtapeCommerciale()
    {
        return $this->etapeCommerciale;
    }
}
