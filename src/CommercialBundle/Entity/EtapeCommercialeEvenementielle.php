<?php

namespace CommercialBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * EtapeCommercialeEvenementielle
 *
 * @ORM\Table(name="etape_commerciale_evenementielle")
 * @ORM\Entity(repositoryClass="CommercialBundle\Repository\EtapeCommercialeEvenementielleRepository")
 */
class EtapeCommercialeEvenementielle extends AbstractEtapeCommerciale
{
    /**
      * @var ActionCommercialeEvenementielle
      *
      * @ORM\ManyToOne(targetEntity="CommercialBundle\Entity\ActionCommercialeEvenementielle", inversedBy="etapeCommerciale",cascade={"persist"})
      * @ORM\JoinColumn(nullable=false)
      */
    private $actionCommerciale;

    /**
     * @var int
     *
     * @ORM\Column(name="delaiJours", type="integer")
     */
    private $delaiJours;

    /**
     * Set delaiJours
     *
     * @param integer $delaiJours
     *
     * @return EtapeCommercialeEvenementielle
     */
    public function setDelaiJours($delaiJours)
    {
        $this->delaiJours = $delaiJours;

        return $this;
    }

    /**
     * Get delaiJours
     *
     * @return int
     */
    public function getDelaiJours()
    {
        return $this->delaiJours;
    }

    /**
     * Set actionCommerciale
     *
     * @param \CommercialBundle\Entity\ActionCommercialeEvenementielle $actionCommerciale
     *
     * @return EtapeCommercialeEvenementielle
     */
    public function setActionCommerciale(\CommercialBundle\Entity\ActionCommercialeEvenementielle $actionCommerciale)
    {
        $this->actionCommerciale = $actionCommerciale;

        return $this;
    }

    /**
     * Get actionCommerciale
     *
     * @return \CommercialBundle\Entity\ActionCommercialeEvenementielle
     */
    public function getActionCommerciale()
    {
        return $this->actionCommerciale;
    }
}
