<?php

namespace CommercialBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Symfony\Component\Validator\Constraints as Assert;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * AbstractActionCommerciale
 *
 * @ORM\Table(name="action_commerciale")
 * @ORM\Entity(repositoryClass="CommercialBundle\Repository\AbstractActionCommercialeRepository")
 * @ORM\InheritanceType("SINGLE_TABLE")
 * @ORM\DiscriminatorColumn( name = "discr", type = "string" )
 * @ORM\DiscriminatorMap({ "permanente" = "ActionCommercialePermanente","evenementielle" = "ActionCommercialeEvenementielle" })
 *
 * @Vich\Uploadable
 *
 */

abstract class AbstractActionCommerciale
{
    use TimestampableEntity;

    const PERMANENTE = 1;
    const EVENEMENTIELLE = 2;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="titre", type="string", length=255)
     */
    private $titre;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text")
     */
    private $description;

    /**
     * @var User $auteur
     *
     * @Gedmo\Blameable(on="create")
     * @ORM\ManyToOne(targetEntity="Application\Sonata\UserBundle\Entity\User", inversedBy="actionCommerciale")
     * @ORM\JoinColumn(referencedColumnName="id")
     */
    protected $auteur;

     /**
      * @Vich\UploadableField(mapping="commerciale_image", fileNameProperty="imagePrincipaleName")
      *
      * @var File
      */
    private $imagePrincipale;

    /**
     * @ORM\Column(type="string", length=255)
     *
     * @var string
     */
    private $imagePrincipaleName;

    /**
     * @Vich\UploadableField(mapping="commerciale_image", fileNameProperty="imageSecondaireName")
     *
     * @var File
     */
    private $imageSecondaire;

    /**
     * @ORM\Column(type="string", length=255)
     *
     * @var string
     */
    private $imageSecondaireName;

    /**
     * @var int
     * @Assert\Range(
     *      min = 0,
     *      max = 5,
     *      minMessage = "Minimum Difficulty: {{ limit }}",
     *      maxMessage = "Maximum Difficulty {{ limit }}"
     * )
     *
     * @ORM\Column(name="difficulte", type="integer")
     */
    private $difficulte;

    /**
     * @var \Application\Sonata\ClassificationBundle\Entity\Category
     *
     * @ORM\ManyToOne(targetEntity="Application\Sonata\ClassificationBundle\Entity\Category")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="category_id", referencedColumnName="id")
     * })
     */
    private $categorie;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set titre
     *
     * @param string $titre
     *
     * @return AbstractActionCommerciale
     */
    public function setTitre($titre)
    {
        $this->titre = $titre;

        return $this;
    }

    /**
     * Get titre
     *
     * @return string
     */
    public function getTitre()
    {
        return $this->titre;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return AbstractActionCommerciale
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set imagePrincipale
     *
     * @param string $imagePrincipale
     *
     * @return AbstractActionCommerciale
     */
    public function setImagePrincipale($imagePrincipale)
    {
        $this->imagePrincipale = $imagePrincipale;

        return $this;
    }

    /**
     * Get imagePrincipale
     *
     * @return string
     */
    public function getImagePrincipale()
    {
        return $this->imagePrincipale;
    }

    /**
     * Set imageSecondaire
     *
     * @param string $imageSecondaire
     *
     * @return AbstractActionCommerciale
     */
    public function setImageSecondaire($imageSecondaire)
    {
        $this->imageSecondaire = $imageSecondaire;

        return $this;
    }

    /**
     * Get imageSecondaire
     *
     * @return string
     */
    public function getImageSecondaire()
    {
        return $this->imageSecondaire;
    }

    /**
     * Set difficulte
     *
     * @param integer $difficulte
     *
     * @return AbstractActionCommerciale
     */
    public function setDifficulte($difficulte)
    {
        $this->difficulte = $difficulte;

        return $this;
    }

    /**
     * Get difficulte
     *
     * @return int
     */
    public function getDifficulte()
    {
        return $this->difficulte;
    }

    /**
     * Set imagePrincipaleName
     *
     * @param string $imagePrincipaleName
     *
     * @return AbstractActionCommerciale
     */
    public function setImagePrincipaleName($imagePrincipaleName)
    {
        $this->imagePrincipaleName = $imagePrincipaleName;

        return $this;
    }

    /**
     * Get imagePrincipaleName
     *
     * @return string
     */
    public function getImagePrincipaleName()
    {
        return $this->imagePrincipaleName;
    }

    /**
     * Set imageSecondaireName
     *
     * @param string $imageSecondaireName
     *
     * @return AbstractActionCommerciale
     */
    public function setImageSecondaireName($imageSecondaireName)
    {
        $this->imageSecondaireName = $imageSecondaireName;

        return $this;
    }

    /**
     * Get imageSecondaireName
     *
     * @return string
     */
    public function getImageSecondaireName()
    {
        return $this->imageSecondaireName;
    }

    /**
     * Set auteur
     *
     * @param \Application\Sonata\UserBundle\Entity\User $auteur
     *
     * @return AbstractActionCommerciale
     */
    public function setAuteur(\Application\Sonata\UserBundle\Entity\User $auteur = null)
    {
        $this->auteur = $auteur;

        return $this;
    }

    /**
     * Get auteur
     *
     * @return \Application\Sonata\UserBundle\Entity\User
     */
    public function getAuteur()
    {
        return $this->auteur;
    }

    /**
     * Set categorie
     *
     * @param \Application\Sonata\ClassificationBundle\Entity\Category $categorie
     *
     * @return AbstractActionCommerciale
     */
    public function setCategorie(\Application\Sonata\ClassificationBundle\Entity\Category $categorie = null)
    {
        $this->categorie = $categorie;

        return $this;
    }

    /**
     * Get categorie
     *
     * @return \Application\Sonata\ClassificationBundle\Entity\Category
     */
    public function getCategorie()
    {
        return $this->categorie;
    }
}
