<?php

namespace CommercialBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ActionCommercialePermanente
 *
 * @ORM\Table(name="permanente")
 * @ORM\Entity(repositoryClass="CommercialBundle\Repository\ActionCommercialePermanenteRepository")
 */

class ActionCommercialePermanente extends AbstractActionCommerciale
{
    /**
     *
     * @ORM\OneToMany(targetEntity="CommercialBundle\Entity\EtapeCommercialePermanente", mappedBy="actionCommerciale", cascade={"persist", "remove"},orphanRemoval=true)
     * @ORM\OrderBy({"date" = "ASC"})
     */
    protected $etapeCommerciale;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->etapeCommerciale = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add etapeCommerciale
     *
     * @param \CommercialBundle\Entity\EtapeCommercialePermanente $etapeCommerciale
     *
     * @return ActionCommercialePermanente
     */
    public function addEtapeCommerciale(\CommercialBundle\Entity\EtapeCommercialePermanente $etapeCommerciale)
    {
        $this->etapeCommerciale[] = $etapeCommerciale;

        return $this;
    }

    /**
     * Remove etapeCommerciale
     *
     * @param \CommercialBundle\Entity\EtapeCommercialePermanente $etapeCommerciale
     */
    public function removeEtapeCommerciale(\CommercialBundle\Entity\EtapeCommercialePermanente $etapeCommerciale)
    {
        $this->etapeCommerciale->removeElement($etapeCommerciale);
    }

    /**
     * Get etapeCommerciale
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getEtapeCommerciale()
    {
        return $this->etapeCommerciale;
    }
}
