<?php

namespace CommercialBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * EtapeCommercialePermanente
 *
 * @ORM\Table(name="etape_commerciale_permanente")
 * @ORM\Entity(repositoryClass="CommercialBundle\Repository\EtapeCommercialePermanenteRepository")
 */
class EtapeCommercialePermanente extends AbstractEtapeCommerciale
{
    /**
      * @var ActionCommercialePermanente
      *
      * @ORM\ManyToOne(targetEntity="CommercialBundle\Entity\ActionCommercialePermanente", inversedBy="etapeCommerciale",cascade={"persist"})
      * @ORM\JoinColumn(nullable=false)
      */
    private $actionCommerciale;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date", type="date")
     */
    private $date;


    /**
     * Set date
     *
     * @param \DateTime $date
     *
     * @return EtapeCommercialePermanente
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set actionCommerciale
     *
     * @param \CommercialBundle\Entity\ActionCommercialePermanente $actionCommerciale
     *
     * @return EtapeCommercialePermanente
     */
    public function setActionCommerciale(\CommercialBundle\Entity\ActionCommercialePermanente $actionCommerciale)
    {
        $this->actionCommerciale = $actionCommerciale;

        return $this;
    }

    /**
     * Get actionCommerciale
     *
     * @return \CommercialBundle\Entity\ActionCommercialePermanente
     */
    public function getActionCommerciale()
    {
        return $this->actionCommerciale;
    }
}
