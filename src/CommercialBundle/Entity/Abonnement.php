<?php

namespace CommercialBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Doctrine\ORM\Mapping\UniqueConstraint;

/**
 * Abonnement
 *
 * @ORM\Table(name="abonnement", uniqueConstraints={@UniqueConstraint(name="abonnement_unique", columns={"abonne_id", "action_commerciale_id"})}))
 * @ORM\Entity(repositoryClass="CommercialBundle\Repository\AbonnementRepository")
 */
class Abonnement
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var User $abonne
     *
     * @Gedmo\Blameable(on="create")
     * @ORM\ManyToOne(targetEntity="Application\Sonata\UserBundle\Entity\User", inversedBy="abonnement")
     * @ORM\JoinColumn(referencedColumnName="id")
     */
    protected $abonne;

    /**
     * @ORM\ManyToOne(targetEntity="CommercialBundle\Entity\AbstractActionCommerciale", cascade={"persist"})
     */
    private $actionCommerciale;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_de_fin", type="datetime", nullable=true)
     */
    private $dateDeFin;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="prochain_date_execution", type="date", nullable=true)
     */
    private $prochainDateExecution;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set prochainDateExecution
     *
     * @param \DateTime prochainDateExecution
     *
     * @return Abonnement
     */
    public function setProchainDateExecution($prochainDateExecution)
    {
        $this->prochainDateExecution = $prochainDateExecution;

        return $this;
    }

    /**
     * Get prochainDateExecution
     *
     * @return \DateTime
     */
    public function getProchainDateExecution()
    {
        return $this->prochainDateExecution;
    }

    /**
     * Set dateDeFin
     *
     * @param \DateTime $dateDeFin
     *
     * @return Abonnement
     */
    public function setDateDeFin($dateDeFin)
    {
        $this->dateDeFin = $dateDeFin;

        return $this;
    }

    /**
     * Get dateDeFin
     *
     * @return \DateTime
     */
    public function getDateDeFin()
    {
        return $this->dateDeFin;
    }

    /**
     * Set abonne
     *
     * @param \Application\Sonata\UserBundle\Entity\User $abonne
     *
     * @return Abonnement
     */
    public function setAbonne(\Application\Sonata\UserBundle\Entity\User $abonne = null)
    {
        $this->abonne = $abonne;

        return $this;
    }

    /**
     * Get abonne
     *
     * @return \Application\Sonata\UserBundle\Entity\User
     */
    public function getAbonne()
    {
        return $this->abonne;
    }

    /**
     * Set actionCommerciale
     *
     * @param \CommercialBundle\Entity\AbstractActionCommerciale $actionCommerciale
     *
     * @return Abonnement
     */
    public function setActionCommerciale(\CommercialBundle\Entity\AbstractActionCommerciale $actionCommerciale = null)
    {
        $this->actionCommerciale = $actionCommerciale;

        return $this;
    }

    /**
     * Get actionCommerciale
     *
     * @return \CommercialBundle\Entity\AbstractActionCommerciale
     */
    public function getActionCommerciale()
    {
        return $this->actionCommerciale;
    }
}
