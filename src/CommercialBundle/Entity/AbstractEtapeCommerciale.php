<?php

namespace CommercialBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * AbstractEtapeCommerciale
 *
 * @ORM\Table(name="etape_commerciale")
 * @ORM\Entity(repositoryClass="CommercialBundle\Repository\AbstractEtapeCommercialeRepository")
 * @ORM\InheritanceType("SINGLE_TABLE")
 * @ORM\DiscriminatorColumn( name = "discr", type = "string" )
 * @ORM\DiscriminatorMap({ "permanente" = "EtapeCommercialePermanente","evenementielle" = "EtapeCommercialeEvenementielle" })
 */

abstract class AbstractEtapeCommerciale
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="titre", type="string", length=255)
     */
    private $titre;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=255, nullable=true)
     */
    private $description;

    /**
     * @var string
     *
     * @ORM\Column(name="mailBody", type="string", length=255, nullable=true)
     */
    private $mailBody;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set titre
     *
     * @param string $titre
     *
     * @return AbstractEtapeCommerciale
     */
    public function setTitre($titre)
    {
        $this->titre = $titre;

        return $this;
    }

    /**
     * Get titre
     *
     * @return string
     */
    public function getTitre()
    {
        return $this->titre;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return AbstractEtapeCommerciale
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set mailBody
     *
     * @param string $mailBody
     *
     * @return AbstractEtapeCommerciale
     */
    public function setMailBody($mailBody)
    {
        $this->mailBody = $mailBody;

        return $this;
    }

    /**
     * Get mailBody
     *
     * @return string
     */
    public function getMailBody()
    {
        return $this->mailBody;
    }
}
