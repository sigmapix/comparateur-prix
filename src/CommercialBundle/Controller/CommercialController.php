<?php

namespace CommercialBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use CommercialBundle\Entity\ActionCommercialePermanente;
use CommercialBundle\Entity\ActionCommercialeEvenementielle;
use CommercialBundle\Entity\AbstractActionCommerciale;
use CommercialBundle\Entity\Abonnement;
use CommercialBundle\Form\AbonnementType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;

class CommercialController extends Controller
{
    /**
     * @Route("/")
     */
    public function indexAction()
    {
        return $this->render('CommercialBundle:Commercial:index.html.twig');
    }

    /**
     * @Route("/permanent", name="permanent")
     */
    public function permanentAction(Request $request)
    {
      $repository =  $this->getDoctrine()->getManager()->getRepository(ActionCommercialePermanente::class);
      $actionsCommerciales = $repository->findAll();

      return $this->render('CommercialBundle:Commercial:list.html.twig', array(
        'actionsCommerciales' => $actionsCommerciales,
      ));
    }

    /**
     * @Route("/evenementielle", name="evenementielle")
     */
    public function evenementielleAction(Request $request)
    {
      $repository =  $this->getDoctrine()->getManager()->getRepository(ActionCommercialeEvenementielle::class);
      $actionsCommerciales = $repository->findAll();
      return $this->render('CommercialBundle:Commercial:list.html.twig', array(
        'actionsCommerciales' => $actionsCommerciales,
      ));
    }

    /**
     * @Route("/abonnement", name="abonnement")
     */
    public function abonnementAction(Request $request)
    {
      $user = $this->getUser();
      if($user === null){
        return $this->redirectToRoute('fos_user_security_login');
      }

      $repository =  $this->getDoctrine()->getManager()->getRepository(Abonnement::class);
      $abonnements = $repository->findBy(array('abonne' => $user));
      return $this->render('CommercialBundle:Commercial:abonnement.html.twig', array(
        'abonnements' => $abonnements,
      ));
    }

    /**
     * @Route("/actioncommerciale/{id}", name="actioncommerciale_show",
     *  requirements={"id" = "\d+"}
     * )
     * @ParamConverter("actionCommerciale", class="CommercialBundle:AbstractActionCommerciale")
     */
    public function showAction(Request $request, AbstractActionCommerciale $actionsCommerciale = null)
    {
      if($actionsCommerciale === null){
          throw $this->createNotFoundException('The action commerciale does not exist');
      }

      $user = $this->getUser();
      $repository =  $this->getDoctrine()->getManager()->getRepository(Abonnement::class);
      $abonnement = $repository->findOneBy(array('abonne' => $user, 'actionCommerciale' => $actionsCommerciale));

      $type = AbstractActionCommerciale::PERMANENTE;
      $form = null;
      if($actionsCommerciale instanceof ActionCommercialeEvenementielle){
        $type = AbstractActionCommerciale::EVENEMENTIELLE;
        $form = $this->createForm(AbonnementType::class)->createView();
      }

      return $this->render('CommercialBundle:Commercial:show.html.twig', array(
          'actionsCommerciale' => $actionsCommerciale,
          'abonnement' => $abonnement,
          'type' => $type,
          'form' => $form,
      ));

    }

    /**
     * @Route("/abonner/{id}", name="abonner",
     *  requirements={"id" = "\d+"}
     * )
     * @ParamConverter("actionCommerciale", class="CommercialBundle:AbstractActionCommerciale")
     */
    public function abonnerAction(Request $request, AbstractActionCommerciale $actionsCommerciale = null)
    {
      if($actionsCommerciale === null){
          throw $this->createNotFoundException('The action commerciale does not exist');
      }

      //if user is not logged in, we redirect
      $user = $this->getUser();
      if($user === null){
        return $this->redirectToRoute('fos_user_security_login');
      }

      $today = new \DateTime();

      //create new abonnement
      $abonnement = new Abonnement();
      $abonnement->setAbonne($user);
      $abonnement->setActionCommerciale($actionsCommerciale);

      /*
      * if ActionCommercialePermanente, we do not need to add dateDeFin
      * the dateDeFin and prochainDateExecution is set to null
      *
      * if ActionCommercialeEvenementielle, we need to get the date input of the user
      * once we get the dateDeFin, we set it
      * As for the prochainDateExecution, we retrieve the EtapeEvenementielle which are related
      * We get the "delaiJour" and if it is less than zero, we subtract the number of days from todays date
      * If the date is still not passed, we set the date execution as that date
      * If the date is passed, we loop to the next delaiJour NOTICE the "continue"
      *
      * Once the date is set , we break from the loop and persist the object
      *
      */
      if($actionsCommerciale instanceof ActionCommercialeEvenementielle){
            if ($request->isMethod('POST')) {
            $form = $request -> get('abonnement');
            $date = $form['date'];
            $dateDeFin = new \DateTime($date);
            $abonnement->setDateDeFin($dateDeFin);

            $dateDeFin = $dateDeFin->format('Y-m-d');
            $dateExecution = new \DateTime($dateDeFin);

            $leastDelaiJour = null;

            foreach($actionsCommerciale->getEtapeCommerciale() as $etapeCommerciale){
                $delaiJour = $etapeCommerciale->getDelaiJours();
                $dateExecution = new \DateTime($dateDeFin);
                if($delaiJour > 0) {
                  $dateExecution ->add(new \DateInterval('P'.$delaiJour.'D'));
                }
                elseif($delaiJour < 0) {
                  $delaiJour = abs($delaiJour);
                  $dateExecution ->sub(new \DateInterval('P'.$delaiJour.'D'));
                  if($dateExecution->format('Y-m-d') < $today->format('Y-m-d')){
                    continue;
                  }
                }
                $abonnement->setProchainDateExecution($dateExecution);
                break;
            }
        }
      }

      $em = $this->getDoctrine()->getManager();
      $em->persist($abonnement);
      $em->flush();

      $this->addFlash('notice', 'Vous etes maintenant abonner a cette action.');

      return $this->redirectToRoute('actioncommerciale_show', ['id' => $actionsCommerciale->getId()]);

    }

    /**
     * @Route("/desabonner/{id}", name="desabonner",
     *  requirements={"id" = "\d+"}
     * )
     * @ParamConverter("abonnement", class="CommercialBundle:Abonnement")
     */
    public function desabonnerAction(Request $request, Abonnement $abonnement = null)
    {
      if($abonnement === null){
          throw $this->createNotFoundException('The action commerciale does not exist');
      }

      $user = $this->getUser();
      if($user === null){
        return $this->redirectToRoute('fos_user_security_login');
      }

      $em = $this->getDoctrine()->getManager();
      $em->remove($abonnement);
      $em->flush();

      $this->addFlash('notice', 'Vous etes maintenant desabonner de cette action.');

      return $this->redirectToRoute('actioncommerciale_show', ['id' => $abonnement->getActionCommerciale()->getId()]);

    }
}
