<?php
namespace CommercialBundle\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use CommercialBundle\Entity\EtapeCommercialePermanente;
use CommercialBundle\Entity\Abonnement;
use CommercialBundle\Entity\ActionCommercialeEvenementielle;

class SendNotificationCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
        ->setName('commercial:send_notification')

        ->setDescription('Envoie des notifications')

        ->setHelp('Envoie des notifications aux abonne de action commerciale')
      ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln('Debut de lenvoie des notifs!');

        $em = $this->getContainer()->get('doctrine')->getManager();

        $repositoryEtapePermanente =  $em ->getRepository(EtapeCommercialePermanente::class);
        $repositoryAbonnement =  $em->getRepository(Abonnement::class);

        /*notifications for actions permanente*/

        //retrieve etapePermanente whose date is today
        $today = new \DateTime();
        $etapesPermanentes = $repositoryEtapePermanente->findby(
          array('date' => $today)
        );


        foreach ($etapesPermanentes as $etapesPermanente) {
            //get associated action CommercialBundle
            $actionCommerciale = $etapesPermanente->getActionCommerciale();
            $body = $etapesPermanente->getMailBody();

            if($body !== null){
                //retrieve all abonnements concerned
                $abonnements = $repositoryAbonnement->findBy(array('actionCommerciale' => $actionCommerciale));

                //send mail to each user concerned
                foreach ($abonnements as $abonnement) {
                    $abonne = $abonnement->getAbonne();
                    $toEmail = $abonne->getEmail();
                    $title = "notification";

                    $this->sendMessage($title,$toEmail,$body);
                }
            }


        }

        /*notifications for actions evenementielle*/

        //retrieve all abonnement with a prochainDateExecution same as today
        $today = new \DateTime();
        $abonnements = $repositoryAbonnement->findby(
          array('prochainDateExecution' => $today)
        );

        /*  loop over each abonnement
         *  calculate the days difference between today and the dateDeFin of the abonnement
         *  then we loop over each related etape, if the delaiJour is the same, we execute it
         *  this ensures that even if more that one etape has the same delaiJour, we will still execute it
         *  Once we execute the etape, we need to set the next dateExecution.
         *
         *  We use the variable $actionExecuted, to track if the etape has been executed
         *  There are 3 status: 0 - Not executed , 1 - executed, 2- executed and next etape exists
         *  we set the status to 0 and then start looping other the etape
         *  once the etape is executed, we set the status to 1
         *  on the next iteration, if the delaiJour is not the same as the interval, it will enter the elseif
         *  we check if an action has been executed. if yes, we calculate the next execution date. then we set status to 2
         *
         *  if the status remains 1, this means that there are no new etape
         *  we then remove the abonnement from the database
         */
        foreach ($abonnements as $abonnement) {
            if($abonnement->getActionCommerciale() instanceof ActionCommercialeEvenementielle){
                $dateDeFin = $abonnement->getDateDeFin();
                $interval = $dateDeFin->diff($today)->format('%R%a');
                //removing the + in +0,+1,+2 and so on
                //so we can use strict comparison
                if(!($interval < 0)){
                  $interval = abs($interval);
                }

                $actionCommerciale = $abonnement->getActionCommerciale();
                $etapesCommerciales = $actionCommerciale->getEtapeCommerciale();
                $actionExecuted = 0;
                foreach ($etapesCommerciales as $etapesCommerciale) {
                    if($etapesCommerciale->getDelaiJours() === $interval){
                      $body = $etapesCommerciale->getMailBody();
                          if($body !== null){
                              $abonne = $abonnement->getAbonne();
                              $toEmail = $abonne->getEmail();
                              $title = "notification";

                              $this->sendMessage($title,$toEmail,$body);
                          }
                          $actionExecuted = 1;
                    } else {
                        if($actionExecuted == 1){
                          $delaiJour = $etapesCommerciale->getDelaiJours();
                          $dateDeFin = $abonnement->getDateDeFin();

                          $dateExecution = new \DateTime($dateDeFin->format('Y-m-d'));
                          if($delaiJour > 0) {
                            $dateExecution ->add(new \DateInterval('P'.$delaiJour.'D'));
                          }
                          elseif($delaiJour < 0) {
                            $delaiJour = abs($delaiJour);
                            $dateExecution ->sub(new \DateInterval('P'.$delaiJour.'D'));
                          }
                          $abonnement->setProchainDateExecution($dateExecution);
                          $em->persist($abonnement);
                          $actionExecuted = 2;
                        }
                    }
                }
                if($actionExecuted == 1){
                  $em->remove($abonnement);
                }
            }
        }

        $em->flush();
        $output->writeln('Fin de la mise a jour des status de tous les annonces!');
    }


    private function sendMessage($title,$toEmail,$body)
    {
      $mailer = $this->getContainer()->get('mailer');
      $templating = $this->getContainer()->get('twig');

      $message = (new \Swift_Message($title))
          ->setFrom('esokia.sigmapix@gmail.com')
          ->setTo($toEmail)
          ->setBody(
                $templating->render(
                  'CommercialBundle:Commercial:email.html.twig',
                  array('title' => $title , 'message' => $body)
                ),
                'text/html'
          )
      ;

      $mailer->send($message);
    }
}
