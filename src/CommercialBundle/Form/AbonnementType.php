<?php
namespace CommercialBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

class AbonnementType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
          ->add('date', DateType::class, array(
              'required' => true,
              'widget' => 'single_text'
            ))
          ->add('save', SubmitType::class, array(
              'label' => "Abonner"
            ))
        ;
    }
}
