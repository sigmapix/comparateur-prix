<?php

namespace CommercialBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

use Application\Sonata\UserBundle\Entity\User;
use CommercialBundle\Entity\ActionCommercialePermanente;
use CommercialBundle\Entity\ActionCommercialeEvenementielle;
use CommercialBundle\Entity\EtapeCommercialePermanente;
use CommercialBundle\Entity\EtapeCommercialeEvenementielle;
use CommercialBundle\Entity\Abonnement;

class LoadActionCommercialData extends AbstractFixture implements OrderedFixtureInterface, ContainerAwareInterface
{
    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * {@inheritDoc}
     */
    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
        $this->loadActionCommercial($manager);
        $manager->flush();
    }

    public function loadActionCommercial(ObjectManager $manager)
    {
        $repositoryuser = $manager->getRepository(User::class);
        $user = $repositoryuser->findOneBy(array('username' => 'maxime'));

        $actionCommercialePermanentes = array(
            array('Action Permanent', $user, 'supertest.jpg', '4', 'Etape Permanent', 'Etape Permanent Description', 'Etape Permanent Mail'),
            array('Action Permanente Mail', $user, 'supertest.jpg', '2', 'Etape Permanent', 'Etape Permanent Description', 'Etape Permanent Mail'),
        );

        foreach ($actionCommercialePermanentes as $key => $actionCommercialePermanente) {
            $actionCommercial = new ActionCommercialePermanente();
            $actionCommercial->setTitre($actionCommercialePermanente[0]);
            $actionCommercial->setDescription($actionCommercialePermanente[0]);
            $actionCommercial->setAuteur($actionCommercialePermanente[1]);
            $actionCommercial->setImagePrincipaleName($actionCommercialePermanente[2]);
            $actionCommercial->setImageSecondaireName($actionCommercialePermanente[2]);
            $actionCommercial->setDifficulte($actionCommercialePermanente[3]);

            $etapeCommercialeA = new EtapeCommercialePermanente();
            $etapeCommercialeA->setTitre($actionCommercialePermanente[4].'A');
            $etapeCommercialeA->setDescription(null);
            $etapeCommercialeA->setMailBody(null);
            $etapeCommercialeA->setActionCommerciale($actionCommercial);
            $etapeCommercialeA->setDate(new \DateTime());

            $etapeCommercialeB = new EtapeCommercialePermanente();
            $etapeCommercialeB->setTitre($actionCommercialePermanente[4].'B');
            $etapeCommercialeB->setDescription($actionCommercialePermanente[5]);
            $etapeCommercialeB->setMailBody(null);
            $etapeCommercialeB->setActionCommerciale($actionCommercial);
            $etapeCommercialeB->setDate(new \DateTime());

            $etapeCommercialeC = new EtapeCommercialePermanente();
            $etapeCommercialeC->setTitre($actionCommercialePermanente[4].'C');
            $etapeCommercialeC->setDescription(null);
            $etapeCommercialeC->setMailBody($actionCommercialePermanente[6]);
            $etapeCommercialeC->setActionCommerciale($actionCommercial);
            $etapeCommercialeC->setDate(new \DateTime());

            //create abonnement only for second one
            if($key === 1){
              $abonnement = new Abonnement();
              $abonnement->setAbonne($actionCommercialePermanente[1]);
              $abonnement->setActionCommerciale($actionCommercial);
              $manager->persist($abonnement);
            }

            $manager->persist($actionCommercial);
            $manager->persist($etapeCommercialeA);
            $manager->persist($etapeCommercialeB);
            $manager->persist($etapeCommercialeC);
        }

        $actionCommercialeEvenementielles = array(
            array('Action Evenementielle', $user, 'supertest.jpg', '4', 'Etape Evenementielle', 'Etape Evenementielle Description', 'Etape Evenementielle Mail'),
        );

        foreach ($actionCommercialeEvenementielles as $key => $actionCommercialeEvenementielle) {
            $actionCommercial = new ActionCommercialeEvenementielle();
            $actionCommercial->setTitre($actionCommercialeEvenementielle[0]);
            $actionCommercial->setDescription($actionCommercialeEvenementielle[0]);
            $actionCommercial->setAuteur($actionCommercialeEvenementielle[1]);
            $actionCommercial->setImagePrincipaleName($actionCommercialeEvenementielle[2]);
            $actionCommercial->setImageSecondaireName($actionCommercialeEvenementielle[2]);
            $actionCommercial->setDifficulte($actionCommercialeEvenementielle[3]);

            $etapeCommercialeA = new EtapeCommercialeEvenementielle();
            $etapeCommercialeA->setTitre($actionCommercialeEvenementielle[4].'A');
            $etapeCommercialeA->setDescription(null);
            $etapeCommercialeA->setMailBody(null);
            $etapeCommercialeA->setActionCommerciale($actionCommercial);
            $etapeCommercialeA->setDelaiJours(-1);

            $etapeCommercialeB = new EtapeCommercialeEvenementielle();
            $etapeCommercialeB->setTitre($actionCommercialeEvenementielle[4].'B');
            $etapeCommercialeB->setDescription($actionCommercialeEvenementielle[5]);
            $etapeCommercialeB->setMailBody($actionCommercialeEvenementielle[6]);
            $etapeCommercialeB->setActionCommerciale($actionCommercial);
            $etapeCommercialeB->setDelaiJours(0);

            $etapeCommercialeC = new EtapeCommercialeEvenementielle();
            $etapeCommercialeC->setTitre($actionCommercialeEvenementielle[4].'C');
            $etapeCommercialeC->setDescription($actionCommercialeEvenementielle[5]);
            $etapeCommercialeC->setMailBody(null);
            $etapeCommercialeC->setActionCommerciale($actionCommercial);
            $etapeCommercialeC->setDelaiJours(1);

            //create abonnement only for second one
            if($key === 0){
              $abonnement = new Abonnement();
              $abonnement->setAbonne($actionCommercialePermanente[1]);
              $abonnement->setActionCommerciale($actionCommercial);
              $abonnement->setDateDeFin(new \DateTime());
              $abonnement->setProchainDateExecution(new \DateTime());
              $manager->persist($abonnement);
            }

            $manager->persist($actionCommercial);
            $manager->persist($etapeCommercialeA);
            $manager->persist($etapeCommercialeB);
            $manager->persist($etapeCommercialeC);
        }

        $manager->flush();
    }

    /**
     * Get the order of this fixture
     *
     * @return integer
     */
    public function getOrder()
    {
        return 11;
    }
}
