<?php

namespace CommercialBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Vich\UploaderBundle\Form\Type\VichImageType;

class AbstractActionCommercialeAdmin extends AbstractAdmin
{
    /**
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('id')
            ->add('titre')
            ->add('description')
            ->add('imagePrincipaleName')
            ->add('imageSecondaireName')
            ->add('difficulte')
            ->add('createdAt')
            ->add('updatedAt')
        ;
    }

    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('id')
            ->add('titre')
            ->add('description')
            ->add('imagePrincipaleName')
            ->add('imageSecondaireName')
            ->add('difficulte')
            ->add('createdAt')
            ->add('updatedAt')
            ->add('_action', null, array(
                'actions' => array(
                    'show' => array(),
                    'edit' => array(),
                    'delete' => array(),
                ),
            ))
        ;
    }

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('titre')
            ->add('description')
            -> add('imagePrincipale',VichImageType::class,[
              'required' => false,
              'allow_delete' => false
            ])
            -> add('imageSecondaire',VichImageType::class,[
              'required' => false,
              'allow_delete' => false
            ])
            ->add('difficulte')
            ->add('categorie','sonata_type_model',[
                'multiple' => false,
                'property' => 'name',
                'btn_add'  => false,
            ])
        ;
    }

    /**
     * @param ShowMapper $showMapper
     */
    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->add('id')
            ->add('titre')
            ->add('description')
            ->add('imagePrincipaleName')
            ->add('imageSecondaireName')
            ->add('difficulte')
            ->add('createdAt')
            ->add('updatedAt')
        ;
    }

}
