<?php

namespace CommercialBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;

class ActionCommercialeEvenementielleAdmin extends AbstractActionCommercialeAdmin
{
    /**
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('id')
            ->add('titre')
            ->add('description')
            ->add('imagePrincipaleName')
            ->add('imageSecondaireName')
            ->add('difficulte')
            ->add('createdAt')
            ->add('updatedAt')
        ;
    }

    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('id')
            ->add('titre')
            ->add('description')
            ->add('imagePrincipaleName')
            ->add('imageSecondaireName')
            ->add('difficulte')
            ->add('createdAt')
            ->add('updatedAt')
            ->add('_action', null, array(
                'actions' => array(
                    'show' => array(),
                    'edit' => array(),
                    'delete' => array(),
                ),
            ))
        ;
    }

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        parent::configureFormFields($formMapper);
        $formMapper
          ->add('etapeCommerciale', 'sonata_type_collection', array(
            'type_options' => array( 'delete' => true )), array(
                'by_reference' => true,
                'edit' => 'inline',
                'inline' => 'table',
                'required' => false
        ));
    }

    /**
     * @param ShowMapper $showMapper
     */
    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->add('id')
            ->add('titre')
            ->add('description')
            ->add('imagePrincipaleName')
            ->add('imageSecondaireName')
            ->add('difficulte')
            ->add('createdAt')
            ->add('updatedAt')
        ;
    }

    /**
     * @param mixed $object
     */
    public function prePersist($object)
    {
        foreach ($object->getEtapeCommerciale() as $etapeCommerciale) {
            $etapeCommerciale->setActionCommerciale($object);
        }
    }

    /**
     * @param mixed $object
     */
    public function preUpdate($object)
    {
        foreach ($object->getEtapeCommerciale() as $etapeCommerciale) {
            $etapeCommerciale->setActionCommerciale($object);
        }
    }
}
