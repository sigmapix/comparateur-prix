<?php

namespace CommercialBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;

class EtapeCommercialePermanenteAdmin extends AbstractEtapeCommercialeAdmin
{
    /**
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('id')
            ->add('titre')
            ->add('description')
            ->add('mailBody')
            ->add('date')
        ;
    }

    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('id')
            ->add('titre')
            ->add('description')
            ->add('mailBody')
            ->add('date')
            ->add('_action', null, array(
                'actions' => array(
                    'show' => array(),
                    'edit' => array(),
                    'delete' => array(),
                ),
            ))
        ;
    }

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        parent::configureFormFields($formMapper);
        $startDate = new \DateTime();
        $endDate = new \DateTime();
        $endDate ->add(new \DateInterval('P13M'));
        $format = 'dd/MM/y';
        // dump($startDate);  dump($endDate);
        $formMapper
            ->add('date','sonata_type_date_picker', array(
                    'format'           => $format,
                    'dp_use_current'   => true,
                    'dp_min_date'      => $startDate->format('d/m/Y'),
                    'dp_max_date'      => $endDate->format('d/m/Y'),

            ))
        ;
    }

    /**
     * @param ShowMapper $showMapper
     */
    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->add('id')
            ->add('titre')
            ->add('description')
            ->add('mailBody')
            ->add('date')
        ;
    }
}
