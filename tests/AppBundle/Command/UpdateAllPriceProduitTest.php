<?php
namespace Tests\AppBundle\Command;

use AppBundle\Command\UpdatePriceAllProduit;
use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Component\Console\Tester\CommandTester;

class UpdatePriceAllProduitTest extends \PHPUnit\Framework\TestCase
{

    public function setUp()
    {
      $this->kernel = new \AppKernel('test', true);
      $this->kernel->boot();
    }

    public function testExecuteWithNoArguments()
    {
        $application = new Application($this->kernel);
        $application->add(new UpdatePriceAllProduit());

        $command = $application->find('comparateur:mise_a_jour_produits');
        $commandTester = new CommandTester($command);
        $commandTester->execute(array(
            'command'  => $command->getName(),
        ));

        // the output of the command in the console
        $output = $commandTester->getDisplay();
        $this->assertContains('Debut de la mise a jour des prix de tous les produits!', $output);

        // ...
    }

    public function testExecuteWithArgumentKO()
    {
        $application = new Application($this->kernel);
        $application->add(new UpdatePriceAllProduit());

        $command = $application->find('comparateur:mise_a_jour_produits');
        $commandTester = new CommandTester($command);
        $commandTester->execute(array(
            'command'  => $command->getName(),

            // pass arguments
            'option' => 'KO',
        ));

        // the output of the command in the console
        $output = $commandTester->getDisplay();
        $this->assertContains('Debut de la mise a jour des prix des produits KO!', $output);

        // ...
    }

    public function testExecuteWithArgumentThatDoesNotExist()
    {
        $application = new Application($this->kernel);
        $application->add(new UpdatePriceAllProduit());

        $command = $application->find('comparateur:mise_a_jour_produits');
        $commandTester = new CommandTester($command);
        $commandTester->execute(array(
            'command'  => $command->getName(),

            // pass arguments
            'option' => 'inexistent_option',
        ));

        // the output of the command in the console
        $output = $commandTester->getDisplay();
        $this->assertContains("L'option inexistent_option n'existe pas!", $output);

        // ...
    }
}
