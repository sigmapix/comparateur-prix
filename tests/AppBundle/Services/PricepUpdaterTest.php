<?php
namespace tests\AppBundle\Services;

require_once dirname(__DIR__).'/../../app/AppKernel.php';

use AppBundle\Services\PriceUpdater;
use PHPUnit\Framework\TestCase;

use AppBundle\Entity\Boutique;
use AppBundle\Entity\ProduitHasBoutique;
use AppBundle\Entity\Produit;

class PriceUpdaterTest extends \PHPUnit\Framework\TestCase
{
    /**
    * @var \Symfony\Component\HttpKernel\Kernel
    */
    protected $kernel;

    /**
    * @var \Doctrine\ORM\EntityManager
    */
    protected $entityManager;

    /**
    * @var \Symfony\Component\DependencyInjection\Container
    */
    protected $container;

    public function setUp()
    {
        $this->kernel = new \AppKernel('test', true);
        $this->kernel->boot();

        $this->container = $this->kernel->getContainer();
        $this->entityManager = $this->container->get('doctrine')->getManager();

        //$this->generateSchema();

        parent::setUp();
    }
    
    public function test_Successful_Price_Retrieval_and_Update()
    {
        $time = time();
        $em = $this->entityManager;
        //create a new boutiques, produit and associated hasboutiques
        $boutique = new Boutique;
        $boutique->setNom("altearahTestNormal_".$time);
        $boutique->setCss(".our_price_display .price");
        $em->persist($boutique);

        $produit = new Produit;
        $produit->setNom("ProduitDeTestNormal_".$time);
        $em->persist($produit);

        $produitHasBoutique = new ProduitHasBoutique;
        $produitHasBoutique->setBoutique($boutique);
        $produitHasBoutique->setProduit($produit);
        $produitHasBoutique->setUrl("http://www.altearah.com/boutique/fr/-les-parfums-bio-de-soin/1-parfum-pourpre-bio.html");
        $em->persist($produitHasBoutique);

        //save the entities
        $em->flush();

        //test the priceUpdater to go fetch the price and update the database
        $priceUpdater = new PriceUpdater($em);
        $priceUpdater->updatePrice($produitHasBoutique);

        //assert that correct data is being fetched and saved in database
        $this->assertEquals(29.50, $produitHasBoutique->getPrix());
        $this->assertEquals("29,50 €", $produitHasBoutique->getPrixFullText());
        $this->assertEquals(200, $produitHasBoutique->getStatut());

        //delete entities and revert db back to original state
        $em->remove($boutique);
        $em->remove($produit);
        $em->remove($produitHasBoutique);
        $em->flush();

    }

    public function test_KO_Price_Retrieval_CSS_NOT_FOUND()
    {
        $time = time();
      $em = $this->entityManager;
      //create a new boutiques, produit and associated hasboutiques
      $boutique = new Boutique;
      $boutique->setNom("altearahTestCSS_".$time);
      $boutique->setCss(".thisCssDoesNotExist");
      $em->persist($boutique);

      $produit = new Produit;
      $produit->setNom("ProduitDeTestCSS_".$time);
      $em->persist($produit);

      $produitHasBoutique = new ProduitHasBoutique;
      $produitHasBoutique->setBoutique($boutique);
      $produitHasBoutique->setProduit($produit);
      $produitHasBoutique->setUrl("http://www.altearah.com/boutique/fr/-les-parfums-bio-de-soin/1-parfum-pourpre-bio.html");
      $em->persist($produitHasBoutique);

      //save the entities
      $em->flush();

      //test the priceUpdater to go fetch the price and update the database
      $priceUpdater = new PriceUpdater($em);
      $priceUpdater->updatePrice($produitHasBoutique);

      //assert that the response code is 1000 which which our custom code for this case
      $this->assertEquals(1000, $produitHasBoutique->getStatut());

      //assert that correct data is being inserted in database

      //delete entities and revert db back to original state
      $em->remove($boutique);
      $em->remove($produit);
      $em->remove($produitHasBoutique);
      $em->flush();


    }

    public function test_KO_Price_Retrieval_RESPONSE_CODE_404()
    {
        $time = time();
      $em = $this->entityManager;
      //create a new boutiques, produit and associated hasboutiques
      $boutique = new Boutique;
      $boutique->setNom("altearahTest404_".$time);
      $boutique->setCss(".our_price_display .price");
      $em->persist($boutique);

      $produit = new Produit;
      $produit->setNom("ProduitDeTest404_".$time);
      $em->persist($produit);

      $produitHasBoutique = new ProduitHasBoutique;
      $produitHasBoutique->setBoutique($boutique);
      $produitHasBoutique->setProduit($produit);
      $produitHasBoutique->setUrl("http://httpstat.us/404");
      $em->persist($produitHasBoutique);

      //save the entities
      $em->flush();

      //test the priceUpdater to go fetch the price and update the database
      $priceUpdater = new PriceUpdater($em);
      $priceUpdater->updatePrice($produitHasBoutique);

      //assert that appropriate response code is saved
      $this->assertEquals(404, $produitHasBoutique->getStatut());

      //delete entities and revert db back to original state
      $em->remove($boutique);
      $em->remove($produit);
      $em->remove($produitHasBoutique);
      $em->flush();
    }

    public function test_KO_Price_Retrieval_RESPONSE_CODE_500()
    {
        $time = time();
      $em = $this->entityManager;
      //create a new boutiques, produit and associated hasboutiques
      $boutique = new Boutique;
      $boutique->setNom("altearahTest500_".$time);
      $boutique->setCss(".our_price_display .price");
      $em->persist($boutique);

      $produit = new Produit;
      $produit->setNom("ProduitDeTest500_".$time);
      $em->persist($produit);

      $produitHasBoutique = new ProduitHasBoutique;
      $produitHasBoutique->setBoutique($boutique);
      $produitHasBoutique->setProduit($produit);
      $produitHasBoutique->setUrl("http://httpstat.us/500");
      $em->persist($produitHasBoutique);

      //save the entities
      $em->flush();

      //test the priceUpdater to go fetch the price and update the database
      $priceUpdater = new PriceUpdater($em);
      $priceUpdater->updatePrice($produitHasBoutique);

      //assert that appropriate response code is saved
      $this->assertEquals(500, $produitHasBoutique->getStatut());

      //delete entities and revert db back to original state
      $em->remove($boutique);
      $em->remove($produit);
      $em->remove($produitHasBoutique);
      $em->flush();


    }

    public function test_KO_Price_Retrieval_RESPONSE_CODE_408()
    {
        $time = time();
      $em = $this->entityManager;
      //create a new boutiques, produit and associated hasboutiques
      $boutique = new Boutique;
      $boutique->setNom("altearahTest408_".$time);
      $boutique->setCss(".our_price_display .price");
      $em->persist($boutique);

      $produit = new Produit;
      $produit->setNom("ProduitDeTest408_".$time);
      $em->persist($produit);

      $produitHasBoutique = new ProduitHasBoutique;
      $produitHasBoutique->setBoutique($boutique);
      $produitHasBoutique->setProduit($produit);
      $produitHasBoutique->setUrl("http://httpstat.us/408");
      $em->persist($produitHasBoutique);

      //save the entities
      $em->flush();

      //test the priceUpdater to go fetch the price and update the database
      $priceUpdater = new PriceUpdater($em);
      $priceUpdater->updatePrice($produitHasBoutique);

      //assert that correct data is being fetched
      $this->assertEquals(408, $produitHasBoutique->getStatut());

      //assert that correct data is being inserted in database

      //delete entities and revert db back to original state
      $em->remove($boutique);
      $em->remove($produit);
      $em->remove($produitHasBoutique);
      $em->flush();


    }
    /**
     * @return null
     */
    public function tearDown()
    {
        $this->kernel->shutdown();

        parent::tearDown();
    }
}
