<?php
namespace Tests\AppBundle\Command;

use CommercialBundle\Command\SendNotificationCommand;
use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Component\Console\Tester\CommandTester;
use CommercialBundle\Entity\Abonnement;

class sendNotificationCommandTest extends \PHPUnit\Framework\TestCase
{
  /* The following test depends on the fixture "LoadActionCommercialData.php" */

    public function setUp()
    {
      $this->kernel = new \AppKernel('test', true);
      $this->kernel->boot();
    }

    public function testExecute()
    {
        $application = new Application($this->kernel);
        $application->add(new SendNotificationCommand());

        $container = $this->kernel->getContainer();
        $mailer = $container->get('mailer');
        $logger = new \Swift_Plugins_MessageLogger();
        $mailer->registerPlugin($logger);

        $command = $application->find('commercial:send_notification');
        $commandTester = new CommandTester($command);
        $commandTester->execute(array(
            'command'  => $command->getName(),
        ));

        // the output of the command in the console
        $output = $commandTester->getDisplay();
        $this->assertContains('Debut de lenvoie des notifs!', $output);

        //check if 2 mail is sent (one for permanent, one for evenementielle)
        $this->assertEquals(2, count($logger->getMessages()));

        //check that the next dateDateExecution is set properly to tomorrow (as next etape has delai +1)
        $tomorrow = new \DateTime();
        $tomorrow->add(new \DateInterval('P1D'));
        $em = $container->get('doctrine.orm.entity_manager');
        $repositoryAbonnement =  $em->getRepository(Abonnement::class);
        $abonnement = $repositoryAbonnement->findOneBy(array('prochainDateExecution' => $tomorrow));
        $this->assertNotNull($abonnement);

        //resetting date execution to today so that we can run test again without issue
        $abonnement->setProchainDateExecution(new \DateTime());
        $em->persist($abonnement);
        $em->flush();
    }
}
