<?php

namespace CommercialBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use CommercialBundle\Entity\ActionCommercialePermanente;
use CommercialBundle\Entity\ActionCommercialeEvenementielle;
use CommercialBundle\Entity\AbstractActionCommerciale;
use CommercialBundle\Entity\Abonnement;
use CommercialBundle\Form\AbonnementType;
use Symfony\Component\BrowserKit\Cookie;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Application\Sonata\UserBundle\Entity\User;

class CommercialControllerTest extends WebTestCase
{
    /* The following test depends on the fixture "LoadActionCommercialData.php" */
    
    public function testIndex()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/');

        $this->assertContains('actions evenementielles', $client->getResponse()->getContent());
    }

    public function testPermanent()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/permanent');

        //hardcoding name of the first action permanent
        $this->assertContains('Action Permanent', $client->getResponse()->getContent());
    }

    public function testEvenementielle()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/evenementielle');

        //hardcoding name of the first action evenementielle
        $this->assertContains('Action Evenementielle', $client->getResponse()->getContent());
    }

    public function testCantSeeAbonnementIfNotLoggedIn()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/abonnement');

        //redirecting to login page
        $this->assertContains('<title>Redirecting to /login</title>', $client->getResponse()->getContent());
    }

    public function testShow()
    {
        $client = static::createClient();

        $em = $client->getContainer()->get('doctrine.orm.entity_manager');
        $repositoryActionCommercial = $em->getRepository(ActionCommercialePermanente::class);
        $actionCommercial = $repositoryActionCommercial->findOneBy(array('titre' => 'Action Permanent'));

        $crawler = $client->request('GET', '/actioncommerciale'.'/'.$actionCommercial->getId());

        //hardcoding name of the first action details
        $this->assertContains('Action Permanent', $client->getResponse()->getContent());
        $this->assertContains('difficulte: 4', $client->getResponse()->getContent());
        $this->assertContains('les etapes', $client->getResponse()->getContent());
    }

    public function testLogin()
    {
        $client = static::createClient(array(), array(
            'PHP_AUTH_USER' => 'maxime',
            'PHP_AUTH_PW'   => 'grfdg56',
        ));
        $crawler = $client->request('GET', '/admin/dashboard');
        $this->assertContains('Sonata Admin', $client->getResponse()->getContent());
        $this->assertSame(Response::HTTP_OK, $client->getResponse()->getStatusCode());
    }

    //those two tests need to run one after the other:
    //testAbonnement and testDesAbonnement
    public function testAbonnement(){
        $client = static::createClient(array(), array(
            'PHP_AUTH_USER' => 'maxime',
            'PHP_AUTH_PW'   => 'grfdg56',
        ));

        $em = $client->getContainer()->get('doctrine.orm.entity_manager');
        $repositoryActionCommercial = $em->getRepository(ActionCommercialePermanente::class);
        $actionCommercial = $repositoryActionCommercial->findOneBy(array('titre' => 'Action Permanent'));

        $crawler = $client->request('GET', '/actioncommerciale'.'/'.$actionCommercial->getId());

        $link = $crawler->filter('a:contains("Abonner")') ->eq(0) ->link();

        //user redirected to action commerciale page
        $crawler = $client->click($link);
        $this->assertContains('<title>Redirecting to /actioncommerciale/'.$actionCommercial->getId().'</title>', $client->getResponse()->getContent());

        $em = $client->getContainer()->get('doctrine.orm.entity_manager');
        $repositoryAbonnement = $em->getRepository(Abonnement::class);

        $repositoryuser = $em->getRepository(User::class);
        $user = $repositoryuser->findOneBy(array('username' => 'maxime'));

        //assert record exist in DB
        $abonnement = $repositoryAbonnement->findOneBy(array('abonne' => $user->getId(), 'actionCommerciale' => $actionCommercial->getId()));
        $this->assertNotNull($abonnement );
    }

    public function testDesAbonnement(){
        $client = static::createClient(array(), array(
            'PHP_AUTH_USER' => 'maxime',
            'PHP_AUTH_PW'   => 'grfdg56',
        ));

        $em = $client->getContainer()->get('doctrine.orm.entity_manager');
        $repositoryActionCommercial = $em->getRepository(ActionCommercialePermanente::class);
        $actionCommercial = $repositoryActionCommercial->findOneBy(array('titre' => 'Action Permanent'));

        $crawler = $client->request('GET', '/actioncommerciale'.'/'.$actionCommercial->getId());

        $link = $crawler->filter('a:contains("Desabonner")') ->eq(0) ->link();

        //user redirected to action commerciale page
        $crawler = $client->click($link);
        $this->assertContains('<title>Redirecting to /actioncommerciale/'.$actionCommercial->getId().'</title>', $client->getResponse()->getContent());

        $em = $client->getContainer()->get('doctrine.orm.entity_manager');
        $repositoryAbonnement = $em->getRepository(Abonnement::class);

        $repositoryuser = $em->getRepository(User::class);
        $user = $repositoryuser->findOneBy(array('username' => 'maxime'));

        //assert record does not exist in DB
        $abonnement = $repositoryAbonnement->findOneBy(array('abonne' => $user->getId(), 'actionCommerciale' => $actionCommercial->getId()));
        $this->assertNull($abonnement );
    }

}
