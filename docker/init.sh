#!/bin/bash

. .env

container="${COMPOSE_PROJECT_NAME}_apache_1"
user="www-data"

docker exec -itu ${user} ${container} bash -c "composer install --no-interaction --no-progress"
docker exec -itu ${user} ${container} bash -c "php bin/console doctrine:database:drop --force"
docker exec -itu ${user} ${container} bash -c "php bin/console doctrine:database:create"
docker exec -itu ${user} ${container} bash -c "php bin/console doctrine:migration:migrate -n"
docker exec -itu ${user} ${container} bash -c "php bin/console doctrine:fixtures:load -n"
#docker exec -itu ${user} ${container} bash -c "php bin/console faker:populate"
docker exec -itu ${user} ${container} bash -c "php bin/console assets:install --symlink"
docker exec -itu ${user} ${container} bash -c "php bin/console cache:clear --no-warmup --env=prod"
