Comparateur-Prix
================

#### Application Install
    git clone git@bitbucket.org:sigmapix/comparateur-prix.git
    cd comparateur-prix/
    cp docker/docker-compose.dist.yml docker/docker-compose.yml
Edit containers settings in *docker-compose.yml*.

    docker-compose build
    docker-compose up -d
    ./docker/init.sh

A Symfony project created on July 12, 2017, 9:53 am.
